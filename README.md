# cms-services

## Backend

### Postgres Setup

Download [PostgreSQL](https://www.postgresql.org/) and install to your system.

### Mongodb Setup

Note: This step is only optional if you are using docker to run the formio server.

Download [MongoDB](https://www.mongodb.com/download-center/community), and [install](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/#procedure) it on your system. 

Set up mongoDB to run as a [windows service](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/#run-mongodb-community-edition-as-a-windows-service).

### Redis Setup

There are multiple choices on how you can run redis on a windows system. I have listed out 2 of them.

+ A port of [Redis for Windows](https://github.com/MicrosoftArchive/redis#redis-on-windows). This is what I am currently using.

+ Run using the [Windows Subsystem for Linux](https://redislabs.com/blog/redis-on-windows-10/).

### Node Setup

I recommend installing Node through a version manager. You can use [nvm-windows](https://github.com/coreybutler/nvm-windows#this-is-not-the-same-thing-as-nvm-which-is-a-completely-separate-project-for-maclinux-only) for this purpose.

The version used during the development of cms is `10.15.3`.

### Nodemon Setup

Using npm, install nodemon globally in your system using the command below.

`npm i -g nodemon`

### Clone cms-service

Assuming [Git](https://git-scm.com/) is already installed in your system, clone the cms-service from the BitBucket Repository to a folder in your system.

After the cloning is completed, run the command below.

`npm i`

### Clone Formio

The [Official Formio Repository](https://github.com/formio/formio#a-combined-form-and-api-platform-for-serverless-applications) with the command below.

`git clone https://github.com/formio/formio.git yourDirectory`

To run Formio server in Docker, clone this [fast-docker repository](https://github.com/fast-platform/fast-docker#formio-api-server-docker-container) using the command below.

`git clone https://github.com/fast-platform/fast-docker.git yourDirectory`

Using the second option is recommended for a local development setup.

Replace “yourDirector” with the directory of your choice.

### Configure Formio

When using the official repository clone, the config file is in `./config/default.json`

The main points that require attention here are `{ port, host, domain, mongo }`

`mongo:` This option is where you put the mongoDB connection uri.

This should be in the following format: 

`mongodb://username:password@mongo_host:mongo_port/databse`

When cloning the fast docker, the setup has to create a .env file in the root folder. The template for this is given in the `template.env` file

Here you can set the 
+ `FORMIO_HOST:` The host ip
+ `FORMIO_PORT:` The post to run formio in
+ `ROOT_EMAIL:` The email to use for admin log in
+ `ROOT_PASSWORD:` The password to use for admin login
+ `MONGO:` The mongoDB connection uri

**Note:** Root Email and Root Password gets set during the initial setup, which is skipped if the connection uri given already is a formio db backup.

### Configure cms-service

The template for the config file, `config.example.js`, for cms can be found in the root folder.

Make a copy of this file and rename it as `config.dev.js`.

### Create Database

In PostgreSQL, create a database with the same name as defined in `config.dev.js`.

To clone a database, get a backup file, and restore in your local instance.

No setup is required in MongoDB to set up a fresh database.

To clone a database, use the command below.

`mongodump --archive --uri="<mongo_connection_url>" | mongorestore --archive --nsFrom='02042020_backup.*' --nsTo='formioapp'`
### Migration
Run the migration command below by navigating to the root folder of cms-service.

`npm run migrate`
### Seed
**Note**: This step if only for a fresh database setup. Skip this if the database is cloned.

Run the seed command below by navigating to the root folder of cms-service.

`npm run seed`
### Run Formio
For the official clone, start using the command below.

`npm start`

For the fast-docker clone (assuming you have [Docker](https://www.docker.com/products/docker-desktop) and [Docker Compose](https://github.com/docker/compose) installed), start using the command below. 

`docker-compose up -d`
### Run cms-service
Start the cms-service command using nodemon by running the command below.

`nodemon`

## Frontend:

### Set the environment variables
Set the following variables accordingly
+ `REACT_APP_BASE_URL`: The IP address of the Front End Server
+ `REACT_APP_CMS_PORT`: The port number where the Front End is running
+ `REACT_APP_CMS_BACKEND_URL`: The Base url of the Backend Server
+ `REACT_APP_FORMIO_URL`: The Base url of the Formio Server

### Install Dependencies
run `npm install`

### Start the Front End Server
run `npm start`

### Links

+ [Sails framework documentation](https://sailsjs.com/get-started)
+ [Version notes / upgrading](https://sailsjs.com/documentation/upgrading)
+ [Deployment tips](https://sailsjs.com/documentation/concepts/deployment)
+ [Community support options](https://sailsjs.com/support)
+ [Professional / enterprise options](https://sailsjs.com/enterprise)

### Useful References

+ [Restful API Best Practices](https://phauer.com/2015/restful-api-design-best-practices/)
+ [Web API Pagination with the 'Timestamp_ID' Continuation Token](https://phauer.com/2018/web-api-pagination-timestamp-id-continuation-token/)
+ [Introduction to promises](https://developers.google.com/web/fundamentals/primers/promises)
+ [Scheduling Jobs on a Sails.js Application](https://dzone.com/articles/scheduling-jobs-on-a-sailsjs-application)
+ [How do you get a list of the names of all files present in a directory in Node.js?](https://stackoverflow.com/questions/2727167/how-do-you-get-a-list-of-the-names-of-all-files-present-in-a-directory-in-node-j#2727191)

<!-- Internally, Sails used [`sails-generate@1.16.12`](https://github.com/balderdashy/sails-generate/tree/v1.16.12/lib/core-generators/new). -->



<!--
Note:  Generators are usually run using the globally-installed `sails` CLI (command-line interface).  This CLI version is _environment-specific_ rather than app-specific, thus over time, as a project's dependencies are upgraded or the project is worked on by different developers on different computers using different versions of Node.js, the Sails dependency in its package.json file may differ from the globally-installed Sails CLI release it was originally generated with.  (Be sure to always check out the relevant [upgrading guides](https://sailsjs.com/upgrading) before upgrading the version of Sails used by your app.  If you're stuck, [get help here](https://sailsjs.com/support).)
-->

