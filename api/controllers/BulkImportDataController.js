const { begin, error, end } = sails.helpers.eventLog;
const { assignHttpStatusCode } = sails.helpers.errorHandling;
const { validateRequest } = sails.helpers.validation.http;
const { modelCreate } = sails.helpers.models.crud;
const { bulkImportSchema } = require('../schemas/BulkImportSchema');

const {
  formio: { URL_FORM },
} = sails.config.custom.CONSTANTS;

const {
  getToken,
  requests: { get },
} = sails.helpers.formio;

const bulkImport = async function bulkBeneficiaryDataImport(req, res) {
  const State = await States;
  const Beneficiary = await Beneficiaries;
  const Task = await Tasks;
  let event;
  try {
    event = await begin(req);
    const params = { ...req.allParams() };

    const validatedRequest = await validateRequest({
      body: params,
      schema: bulkImportSchema,
    });

    // Add Mobile Array
    const beneficiaryMobile = validatedRequest.mobile;

    const state = await State.findOne({
      id: validatedRequest.state,
    });

    if (!state) {
      throw new Error('State does not exist');
    }
    const stateName = state.name.toLowerCase().replace(/\s/g, '');

    // create an identification task
    taskType = await Task_type.findOne({
      id: validatedRequest.task_type,
    });

    // Throw an error if taskType is absent
    if (!taskType) {
      throw new Error(`Input Task type is absent in ${state.name}`);
    }

    // fetch the task status
    const taskStatus = await Tasks_status.findOne({
      id: validatedRequest.tasks_status,
    });

    // Throw an error if taskType is absent
    if (!taskStatus) {
      throw new Error(`"Input" Task Status is absent in ${state.name}`);
    }
    const token = await getToken();
    // prepare url
    const url = await sails.helpers.formio.forms.prepareUrl(URL_FORM, `${stateName}-persistent`);

    // Get persistent form
    const persistentForm = await get(token, {}, url);
    let taskcount = 0;
    let validBeneficiaryCount = 0;
    const failedBeneficiary = [];
    const taskfailedBeneficairy = [];
    for (j = 0; j < beneficiaryMobile.length; j += 1) {
      /* eslint-disable no-await-in-loop */
      const beneficiaryNumber = beneficiaryMobile[j];

      let beneficiary = await Beneficiary.findOne({
        mobile: beneficiaryNumber,
      });

      if (!beneficiary) {
        // Prepare Beneficiary Record
        const beneficiaryModel = {};
        beneficiaryModel.mobile = beneficiaryNumber;
        beneficiaryModel.state = state.id;
        beneficiaryModel.source = 'Bulk Import';
        beneficiaryModel.config = {};
        beneficiaryModel.config.pFormPath = `${stateName}/persistent`;
        beneficiaryModel.config.pFormID = persistentForm._id;

        // create beneficiary record
        beneficiary = await modelCreate({
          validatedRequest: {
            ...beneficiaryModel,
          },
          model: Beneficiary,
        });
        if (beneficiary) {
          validBeneficiaryCount += 1;
        }

        // create the task
        const task = await modelCreate({
          validatedRequest: {
            type: taskType.id,
            state: state.id,
            status: taskStatus.id,
            beneficiary: beneficiary.id,
          },
          model: Task,
        });
        if (task) {
          taskcount += 1;
        }
        if (beneficiary && !task) {
          taskfailedBeneficairy.push(beneficiary.mobile);
        }
      } else {
        failedBeneficiary.push(
          beneficiary.mobile,
          (error.error = `Beneficiary already Exist in ${state.name}`),
        );
      }
    }
    // event logging: success
    await end(event);
    return res.ok({
      message: {
        'Total Benficiaries': beneficiaryMobile.length,
        'failed Beneficiary': beneficiaryMobile.length - validBeneficiaryCount,
        'Failed Beneficiaries': failedBeneficiary,
        'Total task Inserted': taskcount,
        'Benefeciary whose task not created': taskfailedBeneficairy,
      },
    });
  } catch (err) {
    await error(
      {
        error: err.toString(),
      },
      // event
    );
    const statusCode = await assignHttpStatusCode(err);
    return res.status(statusCode).json({
      error: err.toString(),
    });
  }
};

module.exports = {
  bulkImport,
};
