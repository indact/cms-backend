/**
 * CovidController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const axios = require('axios');

const { begin, error, end } = sails.helpers.eventLog;
const { assignHttpStatusCode } = sails.helpers.errorHandling;
const { validateRequest } = sails.helpers.validation.http;

const {
  getToken,
  requests: { get, put },
} = sails.helpers.formio;

const {
  BASE_URL,
  FORMIO_PORT,
  FORMIO_URL,
  CONSTANTS: { formio },
} = sails.config.custom;
const baseURL = `${BASE_URL}:${FORMIO_PORT}`;

const {
  registerSchema,
  loginSchema,
  createLanguagesSchema,
  importBeneficiarySchema,
  getBeneficiariesSchema,
  getAllocatedBeneficiariesSchema,
  autoAllocateBeneficiariesSchema,
  assignStatesToCallerSchema,
  updateBeneficiarySchema,
  getSubmissionSchema,
  getCallersSchema,
  bulkRegisterSchema,
  getStatusReportSchema,
  sendPasswordRecoveryEmailSchema,
  resetPasswordSchema,
} = require('../schemas/CovidHelplineSchema');

const register = async function (req, res) {
  let event;
  try {
    event = await begin(req);
    const request = req.allParams();
    const {
      name, email, password, phone, city, languages, state,
    } = await validateRequest({
      body: request,
      schema: registerSchema,
    });
    const userSubmission = await axios({
      method: 'POST',
      baseURL,
      url: formio.URL_COVID_VOLUNTEER_REGISTRATION,
      headers: {
        'Content-Type': 'application/json',
      },
      data: {
        data: {
          name,
          email,
          phoneNumber: phone,
          password,
        },
      },
    });
    const {
      data: { _id: userId },
      headers: { 'x-jwt-token': FormioToken },
    } = userSubmission;
    const { id } = await CovidCaller.create({
      callerName: name,
      callerEmail: email,
      callerCity: city,
      callerData: userId,
      callerState: state,
    }).fetch();
    const languageIds = (
      await CovidLanguage.find({
        languageName: { in: languages },
      })
    ).map(language => language.id);
    await CovidCaller.addToCollection(id, 'languages', languageIds);
    const caller = await CovidCaller.findOne({ id })
      .populate('languages')
      .populate('callerData')
      .populate('states');
    delete caller.callerData.created_at;
    delete caller.callerData.updated_at;
    delete caller.callerData.form;
    delete caller.callerData.data.password;
    const token = await sails.helpers.jwt.createToken(userId);
    await end({ caller, token, FormioToken }, event);
    return res.ok({ data: { caller, token, FormioToken } });
  } catch (err) {
    await error({ error: err.toString() }, event);
    const statusCode = await assignHttpStatusCode(err);
    res.status(statusCode);
    return res.json({ error: err.toString() });
  }
};

const login = async function (req, res) {
  let event;
  try {
    event = await begin(req);
    const request = req.allParams();
    const { email, password } = await validateRequest({
      body: request,
      schema: loginSchema,
    });
    const response = await sails.helpers.covid.login({ email, password });
    await end({ data: response }, event);
    return res.ok({ data: response });
  } catch (err) {
    await error({ error: err.toString() }, event);
    const statusCode = await assignHttpStatusCode(err);
    res.status(statusCode);
    return res.json({ error: err.toString() });
  }
};

const createLanguages = async function (req, res) {
  let event;
  try {
    event = await begin(req);
    const request = req.allParams();
    let { languages } = await validateRequest({
      body: request,
      schema: createLanguagesSchema,
    });
    languages = await CovidLanguage.createEach(
      languages.map(language => ({ languageName: language })),
    ).fetch();
    await end({ data: { languages } }, event);
    return res.ok({ data: { languages } });
  } catch (err) {
    await error({ error: err.toString() }, event);
    const statusCode = await assignHttpStatusCode(err);
    res.status(statusCode);
    return res.json({ error: err.toString() });
  }
};

const getLanguages = async function (req, res) {
  let event;
  try {
    event = await begin(req);
    const languages = await CovidLanguage.find();
    await end({ data: { languages } }, event);
    return res.ok({ data: { languages } });
  } catch (err) {
    await error({ error: err.toString() }, event);
    const statusCode = await assignHttpStatusCode(err);
    res.status(statusCode);
    return res.json({ error: err.toString() });
  }
};

const importBeneficiaries = async function (req, res) {
  let event;
  try {
    event = await begin(req);
    const request = req.allParams();
    const validatedRequest = await validateRequest({
      body: request,
      schema: importBeneficiarySchema,
    });
    let { beneficiaries } = validatedRequest;
    const state = await CovidState.findOne({ stateName: validatedRequest.state });
    if (!state) {
      throw new Error('State Missing');
    }
    const { id: stateId } = state;
    beneficiaries = beneficiaries.map(({
      name, phone, source, location,
    }) => {
      temp = {
        beneficiaryName: name,
        beneficiaryPhone: phone,
        beneficiaryLocation: location,
        source,
        state: stateId,
      };
      return temp;
    });
    const response = await CovidBeneficiary.createEach(beneficiaries).fetch();
    await end(response, event);
    return res.ok({ data: response });
  } catch (err) {
    await error({ error: err.toString() }, event);
    const statusCode = await assignHttpStatusCode(err);
    res.status(statusCode);
    return res.json({ error: err.toString() });
  }
};

const getBeneficiaries = async function (req, res) {
  let event;
  try {
    event = await begin(req);
    const request = req.allParams();
    const { state, caller, unallocated } = await validateRequest({
      body: request,
      schema: getBeneficiariesSchema,
    });
    const criteria = unallocated ? { state, caller: null } : { state, caller };
    const response = await CovidBeneficiary.find(criteria).populate('beneficiaryDataRecord');
    await end(response, event);
    return res.ok({ data: response });
  } catch (err) {
    await error({ error: err.toString() }, event);
    const statusCode = await assignHttpStatusCode(err);
    res.status(statusCode);
    return res.json({ error: err.toString() });
  }
};

const getAllocatedBeneficiaries = async function (req, res) {
  let event;
  try {
    event = await begin(req);
    const request = req.allParams();
    const { state } = await validateRequest({
      body: request,
      schema: getAllocatedBeneficiariesSchema,
    });
    const { beneficiaries } = await CovidCaller.findOne({
      id: req.session.user,
    }).populate('beneficiaries', { state });
    await end(beneficiaries, event);
    return res.ok({ data: { beneficiaries } });
  } catch (err) {
    await error({ error: err.toString() }, event);
    const statusCode = await assignHttpStatusCode(err);
    res.status(statusCode);
    return res.json({ error: err.toString() });
  }
};

const autoAllocateBeneficiaries = async function (req, res) {
  let event;
  try {
    event = await begin(req);
    const request = req.allParams();
    const { state, number } = await validateRequest({
      body: request,
      schema: autoAllocateBeneficiariesSchema,
    });
    const ids = (
      await CovidBeneficiary.find({ where: { caller: null, state }, limit: number })
    ).map(({ id }) => id);
    const beneficiaries = await CovidBeneficiary.update({ id: { in: ids } })
      .set({ caller: req.session.user })
      .fetch();
    await end(beneficiaries, event);
    return res.ok({ data: { beneficiaries } });
  } catch (err) {
    await error({ error: err.toString() }, event);
    const statusCode = await assignHttpStatusCode(err);
    res.status(statusCode);
    return res.json({ error: err.toString() });
  }
};

const assignStatesToCaller = async function (req, res) {
  let event;
  try {
    event = await begin(req);
    const request = req.allParams();
    const { states, caller } = await validateRequest({
      body: request,
      schema: assignStatesToCallerSchema,
    });
    const stateIds = (await CovidState.find({ stateName: { in: states } })).map(state => state.id);
    await CovidCaller.addToCollection(caller, 'states', stateIds);
    await end({ message: 'Assignment Successful' }, event);
    return res.ok({ data: { message: 'Assignment Successful' } });
  } catch (err) {
    await error({ error: err.toString() }, event);
    const statusCode = await assignHttpStatusCode(err);
    res.status(statusCode);
    return res.json({ error: err.toString() });
  }
};

const createStates = async function (req, res) {
  let event;
  try {
    event = await begin(req);
    const states = await CovidState.createEach([
      { stateName: 'Andhra Pradesh' },
      { stateName: 'Arunachal Pradesh' },
      { stateName: 'Assam' },
      { stateName: 'Bihar' },
      { stateName: 'Chhattisgarh' },
      { stateName: 'Delhi' },
      { stateName: 'Goa' },
      { stateName: 'Gujarat' },
      { stateName: 'Haryana' },
      { stateName: 'Himachal Pradesh' },
      { stateName: 'Jammu and Kashmir' },
      { stateName: 'Jharkhand' },
      { stateName: 'Karnataka' },
      { stateName: 'Kerala' },
      { stateName: 'Madhya Pradesh' },
      { stateName: 'Maharashtra' },
      { stateName: 'Manipur' },
      { stateName: 'Meghalaya' },
      { stateName: 'Mizoram' },
      { stateName: 'Nagaland' },
      { stateName: 'Odisha' },
      { stateName: 'Punjab' },
      { stateName: 'Rajasthan' },
      { stateName: 'Sikkim' },
      { stateName: 'Tamil Nadu' },
      { stateName: 'Telangana' },
      { stateName: 'Tripura' },
      { stateName: 'Uttar Pradesh' },
      { stateName: 'Uttarakhand' },
      { stateName: 'West Bengal' },
    ]).fetch();
    await end(states, event);
    return res.ok({ data: states });
  } catch (err) {
    await error({ error: err.toString() }, event);
    const statusCode = await assignHttpStatusCode(err);
    res.status(statusCode);
    return res.json({ error: err.toString() });
  }
};

const getListOfStates = async function (req, res) {
  let event;
  try {
    event = await begin(req);
    const states = await CovidState.find();
    await end({ data: { states } }, event);
    return res.ok({ data: { states } });
  } catch (err) {
    await error({ error: err.toString() }, event);
    const statusCode = await assignHttpStatusCode(err);
    res.status(statusCode);
    return res.json({ error: err.toString() });
  }
};

const updateBeneficiary = async function (req, res) {
  let event;
  try {
    event = await begin(req);
    const request = req.allParams();
    const {
      beneficiaryId: id,
      submissionId: beneficiaryData,
      status,
      callbackDate,
    } = await validateRequest({
      body: request,
      schema: updateBeneficiarySchema,
    });
    const values = { beneficiaryData, status };
    if (callbackDate) {
      values.callbackDate = callbackDate;
    }
    await Submissions.update({ id: beneficiaryData }).set({ beneficiary: id });
    const beneficiary = await CovidBeneficiary.update({ id })
      .set({ beneficiaryData, status, callbackDate })
      .fetch();
    await end({ data: { beneficiary } }, event);
    return res.ok({ data: { beneficiary } });
  } catch (err) {
    await error({ error: err.toString() }, event);
    const statusCode = await assignHttpStatusCode(err);
    res.status(statusCode);
    return res.json({ error: err.toString() });
  }
};

const getSubmission = async function (req, res) {
  let event;
  try {
    event = await begin(req);
    const request = req.allParams();
    const { id } = await validateRequest({
      body: request,
      schema: getSubmissionSchema,
    });
    const submission = await Submissions.findOne({ id });
    await end({ data: { submission } }, event);
    return res.ok({ data: { submission } });
  } catch (err) {
    await error({ error: err.toString() }, event);
    const statusCode = await assignHttpStatusCode(err);
    res.status(statusCode);
    return res.json({ error: err.toString() });
  }
};

const getCallers = async function (req, res) {
  let event;
  try {
    event = await begin(req);
    const request = req.allParams();
    const { name, email } = await validateRequest({
      body: request,
      schema: getCallersSchema,
    });
    const callers = (
      await CovidCaller.find({
        callerName: {
          contains: name || '',
        },
        callerEmail: {
          contains: email || '',
        },
      })
        .populate('languages')
        .populate('states')
        .populate('callerData')
    ).map(caller => ({
      id: caller.id,
      name: caller.callerName,
      email: caller.callerEmail,
      mobile: caller.callerData.data.phoneNumber,
      location: caller.callerCity,
      languages: caller.languages.map(language => language.languageName),
      allocatedStates: caller.states.map(state => state.stateName),
    }));
    await end({ data: { callers } }, event);
    return res.ok({ data: { callers } });
  } catch (err) {
    await error({ error: err.toString() }, event);
    const statusCode = await assignHttpStatusCode(err);
    res.status(statusCode);
    return res.json({ error: err.toString() });
  }
};

const bulkRegister = async function (req, res) {
  let event;
  try {
    event = await begin(req);
    const request = req.allParams();
    const validatedRequest = await validateRequest({
      body: request,
      schema: bulkRegisterSchema,
    });
    Jobs.create('bulkRegister', {
      ...validatedRequest,
    }).save((err) => {
      if (err) throw err;
    });
    await end(`Bulk Registration Scheduled`, event);
    return res.ok(`Bulk Registration Scheduled`);
  } catch (err) {
    await error({ error: err.toString() }, event);
    const statusCode = await assignHttpStatusCode(err);
    res.status(statusCode);
    return res.json({ error: err.toString() });
  }
};

const getStatusReport = async function (req, res) {
  let event;
  try {
    event = await begin(req);
    const request = req.allParams();
    const validatedRequest = await validateRequest({
      body: request,
      schema: getStatusReportSchema,
    });
    const condition = {};
    if (validatedRequest.callers) {
      condition.caller = {
        in: (await CovidCaller.find({ callerEmail: { in: validatedRequest.callers } })).map(
          caller => caller.id,
        ),
      };
    }
    if (validatedRequest.states) {
      condition.state = {
        in: (await CovidState.find({ stateName: { in: validatedRequest.states } })).map(
          state => state.id,
        ),
      };
    }
    const report = {};
    (
      await CovidBeneficiary.find(condition)
        .populate('caller')
        .populate('state')
    ).forEach((item) => {
      if (!item.caller) {
        return;
      }
      const {
        state: { stateName },
        caller: { callerName, callerEmail },
      } = item;
      const label = item.status && item.status.label;
      if (!report[stateName]) {
        report[stateName] = {};
      }
      if (!report[stateName][`${callerName} (${callerEmail})`]) {
        report[stateName][`${callerName} (${callerEmail})`] = {};
      }
      if (!report[stateName][`${callerName} (${callerEmail})`].total) {
        report[stateName][`${callerName} (${callerEmail})`].total = 0;
      }
      if (label) {
        if (!report[stateName][`${callerName} (${callerEmail})`][label]) {
          report[stateName][`${callerName} (${callerEmail})`][label] = 1;
        } else {
          report[stateName][`${callerName} (${callerEmail})`][label] += 1;
        }
      } else if (!report[stateName][`${callerName} (${callerEmail})`]['Call Not Done']) {
        report[stateName][`${callerName} (${callerEmail})`]['Call Not Done'] = 1;
      } else {
        report[stateName][`${callerName} (${callerEmail})`]['Call Not Done'] += 1;
      }
      report[stateName][`${callerName} (${callerEmail})`].total += 1;
    });
    await end({ data: report }, event);
    return res.ok({ data: report });
  } catch (err) {
    await error({ error: err.toString() }, event);
    const statusCode = await assignHttpStatusCode(err);
    res.status(statusCode);
    return res.json({ error: err.toString() });
  }
};

const sendPasswordRecoveryEmail = async function (req, res) {
  let event;
  try {
    event = await begin(req);
    const request = req.allParams();
    const { email } = await validateRequest({
      body: request,
      schema: sendPasswordRecoveryEmailSchema,
    });
    await sails.helpers.covid.sendPasswordRecoveryEmail(email);
    await end({ data: { message: 'Reset Link Sent' } }, event);
    return res.ok({ data: { message: 'Reset Link Sent' } });
  } catch (err) {
    await error({ error: err.toString() }, event);
    const statusCode = await assignHttpStatusCode(err);
    res.status(statusCode);
    return res.json({ error: err.toString() });
  }
};

const resetPassword = async function (req, res) {
  let event;
  try {
    event = await begin(req);
    const request = req.allParams();
    const { password } = await validateRequest({
      body: request,
      schema: resetPasswordSchema,
    });
    const { callerEmail } = await CovidCaller.findOne({ id: req.session.user });
    const token = await getToken();
    const [{ _id: callerId, data }] = await get(
      token,
      {},
      `${FORMIO_URL}/covidvolunteers/submission?data.email=${callerEmail}`,
    );
    data.password = password;
    await put(token, { data }, `${FORMIO_URL}/covidvolunteers/submission/${callerId}`);
    await end({ data: { message: 'Password Reset Successful' } }, event);
    return res.ok({ data: { message: 'Password Reset Successful' } });
  } catch (err) {
    await error({ error: err.toString() }, event);
    const statusCode = await assignHttpStatusCode(err);
    res.status(statusCode);
    return res.json({ error: err.toString() });
  }
};

const webhook = async function (req, res) {
  sails.log(req.allParams());
  res.ok();
};

module.exports = {
  register,
  login,
  createLanguages,
  getLanguages,
  importBeneficiaries,
  getBeneficiaries,
  getAllocatedBeneficiaries,
  autoAllocateBeneficiaries,
  assignStatesToCaller,
  createStates,
  updateBeneficiary,
  getSubmission,
  getListOfStates,
  getCallers,
  bulkRegister,
  webhook,
  getStatusReport,
  sendPasswordRecoveryEmail,
  resetPassword,
};
