/* eslint-disable no-loop-func */
/* eslint-disable no-param-reassign */
/* eslint-disable no-await-in-loop */
/**
 * DataController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const { validateRequest } = sails.helpers.validation.http;
const { begin, end, error } = sails.helpers.eventLog;
const { assignHttpStatusCode } = sails.helpers.errorHandling;

const {
  dataByBeneficiaryCategorySchema,
  dataByTaskStatusSchema,
} = require('../schemas/DataFetchSchema');

const dataByBeneficiaryCategory = async function FetchDataByBeneficiaryCategory(req, res) {
  let event;
  try {
    // event logging: begin
    event = await begin(req);
    // get request parameters
    const request = { ...req.allParams() };

    // validate request
    const validatedRequest = await validateRequest({
      body: request,
      schema: dataByBeneficiaryCategorySchema,
    });

    const { email: user } = req.session.user;
    // schedule job
    Jobs.create('prepBeneficiaryReport', {
      user,
      ...validatedRequest,
    }).save((err) => {
      if (err) throw err;
    });
    const message = `Report Preparation Scheduled. An email will be sent to ${validatedRequest.email
      || user} once the report is prepared`;
    // event logging: success
    await end({ message }, event);
    res.ok({ message });
  } catch (err) {
    // event logging: error
    await error({ error: err.toString() }, event);
    // set the response status according to error
    const statusCode = await assignHttpStatusCode(err);
    // send error response
    return res.status(statusCode).json({ error: err.toString() });
  }
};

const dataByTaskStatus = async function FetchDataByTaskStatus(req, res) {
  let event;
  try {
    // event logging: begin
    event = await begin(req);
    // get request parameters
    const request = { ...req.allParams() };

    // validate request
    const validatedRequest = await validateRequest({
      body: request,
      schema: dataByTaskStatusSchema,
    });

    const { email: user } = req.session.user;
    // schedule job
    Jobs.create('prepTaskReportByStatus', {
      user,
      ...validatedRequest,
    }).save((err) => {
      if (err) throw err;
    });
    const message = `Report Preparation Scheduled. An email will be sent to ${validatedRequest.email
      || user} once the report is prepared`;
    // event logging: success
    await end({ message }, event);
    res.ok({ message });
  } catch (err) {
    // event logging: error
    await error({ error: err.toString() }, event);
    // set the response status according to error
    const statusCode = await assignHttpStatusCode(err);
    // send error response
    return res.status(statusCode).json({ error: err.toString() });
  }
};

module.exports = {
  dataByBeneficiaryCategory,
  dataByTaskStatus,
};
