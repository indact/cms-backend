/**
 * ExotelController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const axios = require('axios');
const camelCase = require('lodash.camelcase');

const { begin, error, end } = sails.helpers.eventLog;
const { assignHttpStatusCode } = sails.helpers.errorHandling;
const { validateRequest } = sails.helpers.validation.http;

const {
  EXOTEL_API_KEY,
  EXOTEL_API_TOKEN,
  EXOTEL_ACCOUNT_SID,
  EXOTEL_SUBDOMAIN,
  EXOPHONE_NUMBER,
  CMS_BASE_URL,
} = sails.config.custom;

const { callMaskingSchema } = require('../schemas/ExotelSchema');

const exotelBaseUrl = `https://${EXOTEL_API_KEY}:${EXOTEL_API_TOKEN}@${EXOTEL_SUBDOMAIN}`;
const formUrlEncoded = x => Object.keys(x).reduce((p, c) => `${p}&${c}=${encodeURIComponent(x[c])}`, '');

const callMasking = async function (req, res) {
  let event;
  try {
    event = await begin(req);
    const requests = req.allParams();
    const { beneficiary } = await validateRequest({
      body: requests,
      schema: callMaskingSchema,
    });

    url = `${exotelBaseUrl}/v1/Accounts/${EXOTEL_ACCOUNT_SID}/Calls/connect.json`;

    const {
      beneficiaryPhone,
      caller: callerId,
      state: { stateName: state },
    } = await CovidBeneficiary.findOne({ id: beneficiary }).populate('state');
    const {
      callerData: {
        data: { phoneNumber: callerPhone },
      },
    } = await CovidCaller.findOne({ id: callerId }).populate('callerData');
    let response;
    axios
      .post(
        url,
        formUrlEncoded({
          From: callerPhone,
          To: beneficiaryPhone,
          CallerId: EXOPHONE_NUMBER[camelCase(state)],
          CallerType: 'trans',
          StatusCallback: `${CMS_BASE_URL}/covidhelpline/callMaskingStatusCallback`,
          'StatusCallbackEvents[]': 'terminal',
        }),
      )
      .then(async ({ data: { Call } }) => {
        const { Sid } = Call;
        const call = { ...Call };
        delete call.Sid;
        response = await CovidExotelCallRecord.create({
          sid: Sid,
          call,
          beneficiary,
        }).fetch();
      });
    await end({ data: response }, event);
    return res.ok();
  } catch (err) {
    await error({ error: err.toString() }, event);
    const statusCode = await assignHttpStatusCode(err);
    res.status(statusCode);
    return res.json({ error: err.toString() });
  }
};

const callMaskingStatusCallback = async function (req, res) {
  let event;
  try {
    event = await begin(req);
    const status = req.allParams();
    const { CallSid } = status;
    delete status.CallSid;
    const {
      call: { Uri },
    } = await CovidExotelCallRecord.findOne({ sid: CallSid });
    axios.get(exotelBaseUrl.concat(Uri)).then(async ({ data: { Call } }) => {
      const call = { ...Call };
      delete call.Sid;
      response = await CovidExotelCallRecord.update({ sid: CallSid }).set({ call, status });
    });
    await end({ data: response }, event);
    return res.ok();
  } catch (err) {
    await error({ error: err.toString() }, event);
    const statusCode = await assignHttpStatusCode(err);
    res.status(statusCode);
    return res.json({ error: err.toString() });
  }
};

module.exports = {
  callMasking,
  callMaskingStatusCallback,
};
