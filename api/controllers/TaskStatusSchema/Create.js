const Joi = require('@hapi/joi');

const schema = Joi.object().keys({
  name: Joi.string().required(),
  state: Joi.string()
    .guid({ version: 'uuidv4' })
    .required(),
  description: Joi.string(),
  category: Joi.string().valid('To Do', 'In Progress', 'Done'),
});

module.exports = {
  schema,
};
