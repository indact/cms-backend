const fs = require('fs');
const camelCase = require('lodash.camelcase');

const { begin, error, end } = sails.helpers.eventLog;
const { assignHttpStatusCode } = sails.helpers.errorHandling;
const { validateRequest } = sails.helpers.validation.http;
const {
  filterTasks, taskBulkAssign, count, exotel,
} = sails.helpers.models.tasks;
const {
  modelCreate,
  modelFetchOne,
  modelRecordNotExists,
  modelRecordAlreadyExists,
  modelUpdate,
} = sails.helpers.models.crud;

const {
  Create,
  Delete,
  FetchOne,
  FetchAll,
  Update,
  BulkAssign,
  TaskQuery,
  Sync,
} = require('./TaskSchema');
const reportDownloadSchema = require('../schemas/ReportDownload');

const {
  getToken,
  requests: { post, put, get },
  submissions: { prepareUrl },
} = sails.helpers.formio;

const {
  formio: { URL_FORM },
} = sails.config.custom.CONSTANTS;

const config = async function (req, res) {
  try {
    return res.ok(sails.i18n.ok);
  } catch (err) {
    return res.badRequest(err);
  }
};

const create = async function (req, res) {
  // declare event
  let event;
  try {
    // initialize event
    event = await begin(req);
    // get request body
    const request = req.allParams();
    // validate the parameters
    // Get the model objects
    const validatedRequest = await validateRequest({
      body: request,
      schema: Create.schema,
    });
    const Task = await Tasks;
    const Type = await Task_type;
    const Status = await Tasks_status;
    const User = await Users;
    const Beneficiary = await Beneficiaries;
    const State = await States;

    await modelRecordNotExists({
      Model: Status,
      name: 'Task Status',
      data: {
        id: validatedRequest.status,
      },
    });
    if ('assignee' in validatedRequest) {
      await modelRecordNotExists({
        Model: User,
        name: 'Assignee',
        data: {
          id: validatedRequest.assignee,
        },
      });
    }
    await modelRecordNotExists({
      Model: Beneficiary,
      name: 'Beneficiary',
      data: {
        id: validatedRequest.beneficiary,
      },
    });
    await modelRecordNotExists({
      Model: Type,
      name: 'Task Type',
      data: {
        id: validatedRequest.type,
      },
    });
    await modelRecordNotExists({
      Model: State,
      name: 'State',
      data: {
        id: validatedRequest.state,
      },
    });
    await modelRecordNotExists({
      Model: Type,
      name: 'Task Type',
      data: {
        id: validatedRequest.type,
      },
    });
    await modelRecordNotExists({
      Model: State,
      name: 'State',
      data: {
        id: validatedRequest.state,
      },
    });

    // Check if the same beneficiary and type task is allready there
    await modelRecordAlreadyExists({
      Model: Task,
      name: 'Task',
      data: {
        type: validatedRequest.type,
        beneficiary: validatedRequest.beneficiary,
      },
    });

    // create Task
    const task = await modelCreate({
      validatedRequest,
      model: Task,
    });
    // event logging: success
    await end(task, event);
    // send response
    return res.ok({
      data: task,
    });
  } catch (err) {
    // event logging: error
    await error(err.toString(), event);
    // set the response status according to error
    const statusCode = await assignHttpStatusCode(err);
    res.status(statusCode);
    // send error response
    return res.json({
      error: err.toString(),
    });
  }
};

const patch = async function updateTaskType(req, res) {
  // declare event
  let event;
  try {
    // event logging: begin
    event = await begin(req);
    // get request data
    const request = {
      ...req.allParams(),
    };
    // validate request
    const validatedRequest = await validateRequest({
      body: request,
      schema: Update.schema,
    });
    const Task = await Tasks;
    const Type = await Task_type;
    const Status = await Tasks_status;
    const User = await Users;
    const Beneficiary = await Beneficiaries;
    const State = await States;
    if ('status' in validatedRequest) {
      await modelRecordNotExists({
        Model: Status,
        name: 'Task Status',
        data: {
          id: validatedRequest.status,
        },
      });
    }
    if ('assignee' in validatedRequest) {
      await modelRecordNotExists({
        Model: User,
        name: 'Assignee',
        data: {
          id: validatedRequest.assignee,
        },
      });
    }
    if ('beneficiary' in validatedRequest) {
      await modelRecordNotExists({
        Model: Beneficiary,
        name: 'Beneficiary',
        data: {
          id: validatedRequest.beneficiary,
        },
      });
    }
    if ('type' in validatedRequest) {
      await modelRecordNotExists({
        Model: Type,
        name: 'Task Type',
        data: {
          id: validatedRequest.type,
        },
      });
    }
    if ('state' in validatedRequest) {
      await modelRecordNotExists({
        Model: State,
        name: 'State',
        data: {
          id: validatedRequest.state,
        },
      });
    }

    if ('type' in validatedRequest && 'beneficiary' in validatedRequest) {
      // Check if the same beneficiary and type task is allready there
      await modelRecordAlreadyExists({
        Model: Task,
        name: 'Task',
        data: {
          type: validatedRequest.type,
          beneficiary: validatedRequest.beneficiary,
        },
      });
    }
    // perform update operation and get response
    const response = await modelUpdate({
      validatedRequest,
      Model: Task,
      name: 'Task',
    });
    // event logging: end
    await end(response, event);
    // return the response
    return res.ok(response);
  } catch (err) {
    // event logging: error
    await error(
      {
        error: err.toString(),
      },
      event,
    );
    // set the response status according to the error
    const statusCode = await assignHttpStatusCode(err);
    // send the error response
    return res.status(statusCode).json({
      error: err.toString(),
    });
  }
};

const deleteTask = async function (req, res) {
  // declare event
  let event;
  try {
    // event logging: begin
    event = await begin(req);
    // get request
    const request = {
      ...req.allParams(),
    };
    // validate request
    const validatedRequest = await validateRequest({
      body: request,
      schema: Delete.schema,
    });

    // const Task = await Tasks;
    // const response = await modelEnableDisable({
    //   validatedRequest,
    //   model: Task,
    // });

    // archive (soft-delete) the record(s)
    const toDelete = await Tasks.archive({ id: { in: validatedRequest.id } });
    /**
     * UsageError: Invalid initial data for new records.
     * Details:
     * Could not use one of the provided new records: Missing value for required attribute `id`.
     * Expected a string, but instead, got: undefined
     *        If the above error is given,
     *        Go to node_modules\waterline\lib\waterline\methods\archive.js
     *        Inside WLModel.find(), when the records are being pushed into the archives array,
     *        Add an id property along to it.
     */

    // create response body
    const response = {
      message: `${toDelete.length} Records Deleted`,
      data: {
        deleted: `${toDelete.length}`,
      },
    };
    // event logging: success
    await end({ response }, event);
    // send response
    return res.ok(response);
  } catch (err) {
    // event logging: error
    await error(err, event);
    // set the response status according to the error
    const statusCode = await assignHttpStatusCode(err);
    // send error response
    return res.status(statusCode).json({ error: err.toString() });
  }
};

const fetchOne = async function (req, res) {
  let event;
  try {
    event = await begin(req);
    const request = {
      ...req.allParams(),
    };
    const validatedRequest = await validateRequest({
      body: request,
      schema: FetchOne.schema,
    });
    const Task = await Tasks;
    const response = await modelFetchOne({
      validatedRequest,
      model: Task,
    });
    await end(
      {
        ...response,
      },
      event,
    );
    return res.ok({
      data: response,
    });
  } catch (err) {
    await error(err, event);
    const statusCode = await assignHttpStatusCode(err);
    res.status(statusCode);
    return res.json(err.toString());
  }
};

const fetchAll = async function (req, res) {
  let event;
  try {
    event = await begin(req);

    const request = req.query;
    const validatedRequest = await validateRequest({
      body: request,
      schema: FetchAll.schema,
    });
    const { response } = await filterTasks(validatedRequest);

    await end({ ...response }, event);
    return res.ok({ ...response });
  } catch (err) {
    await error(
      {
        error: err.toString(),
      },
      event,
    );
    const statusCode = await assignHttpStatusCode(err);
    return res.status(statusCode).json({
      error: err.toString(),
    });
  }
};

const fetchAllMobile = async function fetchAllTasksForMobileApp(req, res) {
  let event;
  try {
    event = await begin(req);

    const request = req.query;
    const validatedRequest = await validateRequest({
      body: request,
      schema: FetchAll.schema,
    });
    // Get List of Tasks
    const { response, submissionList, pSubmissionList } = await filterTasks(validatedRequest);
    // Fetching the submission & pSubmission values
    const submissions = {};
    const pSubmissions = {};

    (
      await Submissions.find({
        id: { in: submissionList },
      })
    ).map((submission) => {
      submissions[submission.id] = submission.data;
      return 0;
    });

    (
      await Submissions.find({
        id: { in: pSubmissionList },
      })
    ).map((pSubmission) => {
      pSubmissions[pSubmission.id] = pSubmission.data;
      return 0;
    });
    // Map submissions and pSubmissions to tasks
    const tasks = response.data.map((task) => {
      const temp = { ...task };
      temp.submission.data = submissions[temp.submission.id] || {};
      temp.beneficiary.form.data = pSubmissions[temp.beneficiary.form.pSubmissionId] || {};
      return temp;
    });

    await end({ ...response, data: tasks }, event);
    return res.ok({ ...response, data: tasks });
  } catch (err) {
    await error(
      {
        error: err.toString(),
      },
      event,
    );
    const statusCode = await assignHttpStatusCode(err);
    return res.status(statusCode).json({
      error: err.toString(),
    });
  }
};

const sync = async function (req, res) {
  let event;
  try {
    event = await begin(req);
    const request = {
      ...req.allParams(),
    };
    const validatedRequest = await validateRequest({
      body: request,
      schema: Sync.schema,
    });

    const Task = await Tasks;
    const Status = await Tasks_status;
    const Type = await Task_type;
    const Beneficiary = await Beneficiaries;
    const { tasks } = validatedRequest;
    let task;

    // Prepare Directory for Conflict Logging
    let directory = './reports';
    if (!fs.existsSync(directory)) {
      fs.mkdirSync(directory);
    }
    directory = './reports/sync';
    if (!fs.existsSync(directory)) {
      fs.mkdirSync(directory);
    }
    directory = './reports/sync/conflict.log';

    for (let i = 0; i < tasks.length; i += 1) {
      /* eslint-disable no-await-in-loop */
      task = tasks[i];
      try {
        if (task.id) {
          const taskObj = await modelFetchOne({
            validatedRequest: task,
            model: Task,
            name: 'Task',
          });

          const taskTypeObj = await modelFetchOne({
            validatedRequest: {
              id: taskObj.type,
            },
            model: Type,
            name: 'Task Type',
          });

          const taskStatusObj = await modelFetchOne({
            validatedRequest: {
              id: taskObj.status,
            },
            model: Status,
            name: 'Task Status',
          });

          const incomingTaskStatusObj = await modelFetchOne({
            validatedRequest: {
              id: task.status,
            },
            model: Status,
            name: 'Task Status',
          });

          const formID = taskTypeObj.form.formId;

          if (task.assignee !== taskObj.assignee) {
            if (taskStatusObj.category === 'Done') {
              // Log Event in conflict.log
              const taskLogAlreadyDone = `\n${new Date().toLocaleString()} :: Task Already Done\n\tTask Id:: ${
                task.id
              }\n\tOriginal Assignee:: ${taskObj.assignee}           New Assignee: ${
                task.assignee
              }`;

              // eslint-disable-next-line no-loop-func
              fs.appendFile(directory, taskLogAlreadyDone, (err2) => {
                // in case of logging error print error to terminal
                if (err2) {
                  sails.log.error(
                    `${new Date().toLocaleString()}:: Logging error for Task Already Done:: ${err2}`,
                  );
                }
              });

              // eslint-disable-next-line no-continue
              continue;
            }

            const taskStatusPriority = {
              'To Do': 1,
              'In Progress': 2,
              Done: 3,
            };

            if (
              taskStatusPriority[taskStatusObj.category]
              <= taskStatusPriority[incomingTaskStatusObj.category]
            ) {
              await modelUpdate({
                validatedRequest: {
                  id: task.id,
                  assignee: task.assignee,
                  status: task.status,
                },
                Model: Task,
                name: 'Task',
              });

              // Log Event in conflict.log
              const taskLogAssigneeStatusOverride = `\n${new Date().toLocaleString()} :: Task Overridden\n\tTask Id:: ${
                task.id
              }\n\tOriginal Assignee:: ${taskObj.assignee}           New Assignee:: ${
                task.assignee
              }\n\tOriginal Status:: ${taskStatusObj.id}           New Status:: ${task.status}`;

              // eslint-disable-next-line no-loop-func
              fs.appendFile(directory, taskLogAssigneeStatusOverride, (err2) => {
                // in case of logging error print error to terminal
                if (err2) {
                  sails.log.error(
                    `${new Date().toLocaleString()}:: Logging error for Task Override:: ${err2}`,
                  );
                }
              });
            }
          } else if (taskObj.status !== task.status) {
            await modelUpdate({
              validatedRequest: {
                id: task.id,
                status: task.status,
              },
              Model: Task,
              name: 'Task',
            });
          }

          // retrieve formio token
          const token = await getToken();
          // prepare url
          let url = await prepareUrl(URL_FORM, formID);

          // post data to formio server and get response
          if (
            task.submission
            && !(
              (typeof task.submission.data.submit !== 'undefined'
                && task.submission.data.submit === null)
              || Object.keys(task.submission.data).length === 0
            )
          ) {
            // Check if task already has a submission id
            // if it already does then update the submission
            if (taskObj.submission_id) {
              await put(
                token,
                {
                  owner: req.session.user.email || req.session.user.mobile,
                  data: {
                    ...task.submission.data,
                  },
                },
                `${url}/${taskObj.submission_id}`,
              );
              // else make a new submission
              // and then update the submission id in the task record
            } else {
              const response = await post(
                token,
                {
                  owner: req.session.user.email || req.session.user.mobile,
                  data: {
                    ...task.submission.data,
                  },
                },
                url,
              );

              // put the new submission id in Task object and save
              await modelUpdate({
                validatedRequest: {
                  id: task.id,
                  submission_id: response.data._id,
                },
                Model: Task,
                name: 'Task',
              });
            }
          }
          if (
            task.pSubmission
            && !(
              (typeof task.pSubmission.data.submit !== 'undefined'
                && task.pSubmission.data.submit === null)
              || Object.keys(task.pSubmission.data).length === 0
            )
          ) {
            const beneficiary = await modelFetchOne({
              validatedRequest: {
                id: taskObj.beneficiary,
              },
              model: Beneficiary,
              name: 'Beneficiary',
            });
            url = await prepareUrl(URL_FORM, beneficiary.config.pFormID);
            // Check if beneficiary already has a pSubmission id
            // if it already does then update the pSubmission
            if (beneficiary.config.pSubmissionId) {
              await put(
                token,
                {
                  owner: req.session.user.email || req.session.user.mobile,
                  data: {
                    ...task.pSubmission.data,
                  },
                },
                `${url}/${beneficiary.config.pSubmissionId}`,
              );
              // else make a new pSubmission
              // and then update the pSubmission id in the beneficiady config
            } else {
              const persistentSubmissionResponse = await post(
                token,
                {
                  owner: req.session.user.email || req.session.user.mobile,
                  data: {
                    ...task.pSubmission.data,
                  },
                },
                url,
              );
              beneficiary.config.pSubmissionId = persistentSubmissionResponse.data._id;
              // updating the beneficiary with the new pSubmission id
              await modelUpdate({
                validatedRequest: {
                  id: taskObj.beneficiary,
                  config: {
                    ...beneficiary.config,
                  },
                },
                Model: Beneficiary,
                name: 'Beneficiaries',
              });
            }
          }
        } else {
          const State = await States;
          const state = await modelFetchOne({
            validatedRequest: {
              id: task.state,
            },
            model: State,
            name: 'State',
          });

          const beneficiaryMobile = task.mobile;
          // Check if the beneficiary allready exits
          const beneficiaries = await Beneficiary.find({
            mobile: beneficiaryMobile,
          });
          let beneficiary = {};
          const token = await getToken();

          // Getting Task Type
          const taskTypeObj = await modelFetchOne({
            validatedRequest: {
              id: task.type,
            },
            model: Type,
            name: 'Task Type',
          });

          const formID = taskTypeObj.form.formId;

          if (beneficiaries.length === 0) {
            const beneficiaryModel = {};
            beneficiaryModel.mobile = beneficiaryMobile;
            beneficiaryModel.state = task.state;
            beneficiaryModel.source = 'Ground Survey';
            beneficiaryModel.config = {};

            beneficiaryModel.config.pFormPath = `${camelCase(state.name)}/persistent`;

            // prepare url
            const url = await sails.helpers.formio.forms.prepareUrl(
              URL_FORM,
              `${camelCase(state.name)}-persistent`,
            );

            const persistentForm = await sails.helpers.formio.requests.get(token, {}, url);
            beneficiaryModel.config.pFormID = persistentForm._id;

            beneficiary = await modelCreate({
              validatedRequest: {
                ...beneficiaryModel,
              },
              model: Beneficiary,
            });
          } else {
            [beneficiary] = beneficiaries;
          }

          let response = {};

          if (
            task.submission
            && !(
              (typeof task.submission.data.submit !== 'undefined'
                && task.submission.data.submit === null)
              || Object.keys(task.submission.data).length === 0
            )
          ) {
            url = await prepareUrl(URL_FORM, formID);
            response = await post(
              token,
              {
                owner: req.session.user.email || req.session.user.mobile,
                data: {
                  ...task.submission.data,
                },
              },
              url,
            );
          }

          // Check if the same beneficiary and type task is already there and resolve conflict
          try {
            await modelRecordAlreadyExists({
              Model: Task,
              name: 'Task',
              data: {
                type: task.type,
                beneficiary: beneficiary.id,
              },
            });

            // Make the submissions for persistentForm
            if (
              task.pSubmission
              && !(
                (typeof task.pSubmission.data.submit !== 'undefined'
                  && task.pSubmission.data.submit === null)
                || Object.keys(task.submission.data).length === 0
              )
            ) {
              url = await prepareUrl(URL_FORM, beneficiary.config.pFormID);
              // Check if beneficiary already has a pSubmission id
              // if it already does then update the pSubmission
              if (beneficiary.config.pSubmissionId) {
                await put(
                  token,
                  {
                    owner: req.session.user.email || req.session.user.mobile,
                    data: {
                      ...task.pSubmission.data,
                    },
                  },
                  `${url}/${beneficiary.config.pSubmissionId}`,
                );
                // else make a new pSubmission
                // and then update the pSubmission id in the beneficiady config
              } else {
                const persistentSubmissionResponse = await post(
                  token,
                  {
                    owner: req.session.user.email || req.session.user.mobile,
                    data: {
                      ...task.pSubmission.data,
                    },
                  },
                  url,
                );
                beneficiary.config.pSubmissionId = persistentSubmissionResponse.data._id;

                await modelUpdate({
                  validatedRequest: {
                    id: beneficiary.id,
                    config: {
                      ...beneficiary.config,
                    },
                  },
                  Model: Beneficiary,
                  name: 'Beneficiaries',
                });
              }
            }

            const taskObj = {
              type: task.type,
              state: task.state,
              status: task.status,
              beneficiary: beneficiary.id,
              assignee: task.assignee || req.session.user.id,
              submission_id: response.data ? response.data._id : '',
            };

            await modelCreate({
              validatedRequest: taskObj,
              model: Task,
            });
          } catch (e) {
            if (e.message === 'Task already exists') {
              const oldTask = await Task.findOne({
                type: task.type,
                beneficiary: beneficiary.id,
              });

              taskStatusPriority = {
                'To Do': 1,
                'In Progress': 2,
                Done: 3,
              };

              const newStatus = (await Tasks_status.findOne({ id: task.status })).category;
              const oldStatus = (await Tasks_status.findOne({ id: oldTask.status })).category;

              if (
                oldStatus !== 'Done'
                && taskStatusPriority[newStatus] >= taskStatusPriority[oldStatus]
              ) {
                // Push the old submission id and pSubmission id into oldSubmissions array
                const { config: temp } = beneficiary;
                if (!temp.oldSubmissions) temp.oldSubmissions = [];
                temp.oldSubmissions.push({
                  submissionId: oldTask.submissionId || '',
                  pSubmissionId: temp.pSubmissionId || '',
                  time: new Date().toLocaleString(),
                  assignee: oldTask.assignee || 'Unassigned',
                });
                delete temp.pSubmissionId;
                // post pSubmission
                if (
                  task.pSubmission
                  && !(
                    (typeof task.pSubmission.data.submit !== 'undefined'
                      && task.pSubmission.data.submit === null)
                    || Object.keys(task.submission.data).length === 0
                  )
                ) {
                  url = await prepareUrl(URL_FORM, temp.pFormID);
                  const persistentSubmissionResponse = await post(
                    token,
                    {
                      owner: req.session.user.email || req.session.user.mobile,
                      data: {
                        ...task.pSubmission.data,
                      },
                    },
                    url,
                  );
                  temp.pSubmissionId = persistentSubmissionResponse.data._id;
                }
                // update beneficiary

                await modelUpdate({
                  validatedRequest: {
                    id: beneficiary.id,
                    config: {
                      ...temp,
                    },
                  },
                  Model: Beneficiary,
                  name: 'Beneficiaries',
                });

                // Then update the old task with the new data
                const taskObj = {
                  status: task.status,
                  assignee: task.assignee || req.session.user.id,
                  submission_id: response.data ? response.data._id : '',
                };

                await modelUpdate({
                  validatedRequest: {
                    id: oldTask.id,
                    ...taskObj,
                  },
                  Model: Task,
                  name: 'Tasks',
                });

                // Could be made into a wrapper function
                const taskLogOverride = `\n${
                  temp.oldSubmissions[temp.oldSubmissions.length - 1].time
                } :: Task Overridden\n\tTask Id:: ${oldTask.id}\n\tOriginal Assignee:: ${
                  oldTask.assignee
                }           New Assignee: ${task.assignee}`;

                // eslint-disable-next-line no-loop-func
                fs.appendFile(directory, taskLogOverride, (err2) => {
                  // in case of logging error print error to terminal
                  if (err2) {
                    sails.log.error(
                      `${new Date().toLocaleString()}:: Logging error for Task Override:: ${err2}`,
                    );
                  }
                });
              } else {
                const taskLogNotOverridden = `\n${new Date().toLocaleString()} :: Task Conflict resolved without override\n\tTask Id:: ${
                  oldTask.id
                }\n\tOriginal Assignee:: ${oldTask.assignee}           Conflicting Assignee: ${
                  task.assignee
                }`;
                // eslint-disable-next-line no-loop-func
                fs.appendFile(directory, taskLogNotOverridden, (err2) => {
                  // in case of logging error print error to terminal
                  if (err2) {
                    sails.log.error(
                      `${new Date().toLocaleString()}:: Logging error for Task Not Overridden:: ${err2}`,
                    );
                  }
                });
              }
            }
          }
        }
      } catch (err) {
        await error(
          {
            error: err.toString(),
          },
          event,
        );
      }
      /* eslint-enable no-await-in-loop */
    }
    return res.ok({
      status: 'ok',
    });
  } catch (err) {
    await error(
      {
        error: err.toString(),
      },
      event,
    );
    const statusCode = await assignHttpStatusCode(err);
    return res.status(statusCode).json({
      error: err.toString(),
    });
  }
};

const prepTaskReport = async function (req, res) {
  // declare event variable
  let event;
  try {
    // initialize the event
    event = await begin(req);
    // get all request parameters
    const request = { ...req.allParams() };
    // validated request parameters
    const validatedRequest = await validateRequest({
      body: request,
      schema: reportDownloadSchema.tasks,
    });
    // get user email from session
    const user = req.session.user.email;
    // schedule job
    Jobs.create('prepTasksReport', {
      user,
      ...validatedRequest,
    }).save((err) => {
      if (err) throw err;
    });
    const response = `Request processed successfully. CSV shall be sent to ${user} shortly.`;
    // event log: end
    await end({ response }, event);
    // response message
    return res.ok({
      message: `Request processed successfully. CSV shall be sent to ${validatedRequest.email
        || req.session.user.email} shortly.`,
    });
  } catch (err) {
    // event log: error
    await error({ error: err.toString() }, event);
    // assign http status code according to error
    const statusCode = await assignHttpStatusCode(err);
    // send error response
    return res.status(statusCode).json({ error: err.toString() });
  }
};

const prepFormReport = async function (req, res) {
  // declare event variable
  let event;
  try {
    // initialize the event
    event = await begin(req);
    // get all request parameters
    const request = { ...req.allParams() };
    // validated request parameters
    const validatedRequest = await validateRequest({
      body: request,
      schema: reportDownloadSchema.forms,
    });
    // get user email from session
    const { email: user } = req.session.user;
    // schedule job
    Jobs.create('prepFormReport', {
      user,
      ...validatedRequest,
    }).save((err) => {
      if (err) throw err;
    });
    const response = `Request processed successfully. CSV shall be sent to ${user} shortly.`;
    // event log: end
    await end({ response }, event);
    // response message
    return res.ok({
      message: `Request processed successfully. CSV shall be sent to ${validatedRequest.email
        || req.session.user.email} shortly.`,
    });
  } catch (err) {
    // event log: error
    await error({ error: err.toString() }, event);
    // assign http status code according to error
    const statusCode = await assignHttpStatusCode(err);
    // send error response
    return res.status(statusCode).json({ error: err.toString() });
  }
};

const download = async function (req, res) {
  // declare event variable
  let event;
  try {
    // initialize the event
    event = await begin(req);
    // get all request parameters
    const request = { ...req.query };
    // validated request parameters
    const validatedRequest = await validateRequest({
      body: request,
      schema: reportDownloadSchema.download,
    });
    const key = Object.keys(validatedRequest)[0];
    // download file path
    const path = key !== 'sync'
        ? `./reports/${key}/${validatedRequest[key]}.csv`
        : `./reports/sync/${validatedRequest[key]}.log`;
    // event log: end
    await end({ ...path }, event);
    return res.download(path, key !== 'sync' ? `${key}-report.csv` : `sync-conflict.log`);
  } catch (err) {
    // event log: error
    await error({ error: err.toString() }, event);
    // assign http status code according to error
    const statusCode = await assignHttpStatusCode(err);
    // send error response
    return res.status(statusCode).json({ error: err.toString() });
  }
};

const patchTask = async function bulkAssignTask(req, res) {
  // declare event
  let event;
  try {
    // event logging: begin
    event = await begin(req);
    // get request data
    const request = {
      ...req.allParams(),
    };
    // validate request

    const validatedRequest = await validateRequest({
      body: request,
      schema: BulkAssign.schema,
    });

    // perform update operation and get response
    const response = await taskBulkAssign(validatedRequest);

    // event logging: end
    await end(response, event);
    // return the response
    return res.ok(response);
  } catch (err) {
    // event logging: error
    await error(
      {
        error: err.toString(),
      },
      event,
    );
    // set the response status according to the error
    const statusCode = await assignHttpStatusCode(err);
    // send the error response
    return res.status(statusCode).json({
      error: err.toString(),
    });
  }
};

const taskCount = async function (req, res) {
  let event;
  try {
    event = await begin(req);

    const request = req.allParams();

    const validatedRequest = await validateRequest({
      body: request,
      schema: TaskQuery.schema,
    });

    if (validateRequest.state_id) {
      const State = await States;

      await modelRecordNotExists({
        Model: State,
        name: 'States',
        data: { id: validatedRequest.state_id },
      });
    }
    if (validatedRequest.assignee) {
      const User = await Users;

      await modelRecordNotExists({
        Model: User,
        name: 'User',
        data: { id: validatedRequest.assignee },
      });
    }

    const response = await count(validatedRequest);

    await end({ ...response }, event);

    return res.ok(response);
  } catch (err) {
    await error({ error: err.toString() }, event);

    const statusCode = await assignHttpStatusCode(err);
    return res.status(statusCode).json({ error: err.toString() });
  }
};

const missedCall = async function missedCall(req, res) {
  const State = await States;
  const Beneficiary = await Beneficiaries;
  const Task = await Tasks;
  let event;
  try {
    event = await begin(req);
    const params = { ...req.allParams() };

    Object.keys(params).forEach((key) => {
      params[key] = params[key].replace(/^\s+|\s+$/g, '');
    });

    params.CallFrom = params.CallFrom.replace(/^0+/, '');

    const beneficiaryMobile = params.CallFrom;

    const state = await State.findOne({
      name: params.state,
    });

    if (!state) {
      throw new Error('State does not exist');
    }

    const stateName = camelCase(state.name);

    let beneficiary = await Beneficiary.findOne({
      mobile: beneficiaryMobile,
    });

    if (!beneficiary) {
      // Prepare Beneficiary Record
      const beneficiaryModel = {};
      beneficiaryModel.mobile = beneficiaryMobile;
      beneficiaryModel.state = state.id;
      beneficiaryModel.source = 'Exotel';
      beneficiaryModel.config = {};

      beneficiaryModel.config.pFormPath = `${stateName}/persistent`;

      const token = await getToken();
      // prepare url
      const url = await sails.helpers.formio.forms.prepareUrl(URL_FORM, `${stateName}-persistent`);

      // Get persistent form
      const persistentForm = await get(token, {}, url);
      beneficiaryModel.config.pFormID = persistentForm._id;
      beneficiaryModel.config.exotelConfig = {
        missedCallCount: 1,
        ...params,
      };

      // initialize a call record
      beneficiaryModel.config.exotelConfig.callRecord = [];
      beneficiaryModel.config.exotelConfig.callRecord.push(params.Created);

      // create beneficiary record
      beneficiary = await modelCreate({
        validatedRequest: {
          ...beneficiaryModel,
        },
        model: Beneficiary,
      });

      // create an identification task
      taskType = await Task_type.findOne({
        name: 'Identification',
        state: state.id,
      });

      // Throw an error if taskType is absent
      if (!taskType) {
        throw new Error(`Identification Task type is absent in ${state.name}`);
      }

      // fetch the task status
      const taskStatus = await Tasks_status.findOne({
        state: state.id,
        name: 'Call Not Done',
      });

      // Throw an error if taskType is absent
      if (!taskStatus) {
        throw new Error(`"Call Not Done" Task Status is absent in ${state.name}`);
      }

      // create the task
      const task = await modelCreate({
        validatedRequest: {
          type: taskType.id,
          state: state.id,
          status: taskStatus.id,
          beneficiary: beneficiary.id,
        },
        model: Task,
      });

      // event logging: success
      await end({ beneficiary, task }, event);

      return res.json('Beneficiary and new Task created');
      // eslint-disable-next-line no-else-return
    }
    const status = await Tasks_status.findOne({
      name: 'Call Not Done',
      state: state.id,
    });

    const escalatedStatus = await Tasks_status.findOne({
      name: 'Escalated',
      state: state.id,
    });

    const beneTaskQuery = `SELECT t.id as task_id,tt.id as task_type_id, ts.id as status_id 
            FROM  tasks t JOIN task_type tt ON t.type = tt.id  JOIN Tasks_status ts ON ts.id = t.status
             WHERE t.deleted=false and t.state ='${state.id}' and t.beneficiary= '${beneficiary.id}'
             and ts.name='Call Not Done'and ts.category in ('To Do','In Progress') ORDER BY tt.sequence DESC limit 1;`;

    const selectedTask = (await sails.sendNativeQuery(beneTaskQuery, [])).rows;

    await modelUpdate({
      validatedRequest: {
        status: escalatedStatus.id,
        id: { in: selectedTask.map(task => task.task_id) },
      },
      Model: Task,
      name: 'Tasks',
    });

    // if Beneficiary already exist
    const { id, config: bConfig } = beneficiary;
    // initiate callRecord if it is not already
    if (!bConfig.exotelConfig) {
      bConfig.exotelConfig = {
        missedCallCount: 0,
        ...params,
      };
      bConfig.exotelConfig.callRecord = [];
    }
    // update missedCallCount
    bConfig.exotelConfig.missedCallCount += 1;
    // push the call time into the call record
    bConfig.exotelConfig.callRecord.push(params.Created);
    // update the beneficiary record
    beneficiary = await modelUpdate({
      validatedRequest: {
        id,
        config: {
          ...bConfig,
        },
      },
      Model: Beneficiary,
      name: 'Beneficiaries',
    });

    await end({ beneficiary }, event);
    return res.json('Beneficiary call record updated');
  } catch (err) {
    await error(
      {
        error: err.toString(),
      },
      event,
    );
    const statusCode = await assignHttpStatusCode(err);
    return res.status(statusCode).json({
      error: err.toString(),
    });
  }
};

const missedCallReport = async function (req, res) {
  let event;
  try {
    event = await begin(req);

    const request = req.allParams();
    const validatedRequest = await validateRequest({
      body: request,
      schema: TaskQuery.schema,
    });
    if (validatedRequest.state_id) {
      const State = await States;

      await modelRecordNotExists({
        Model: State,
        name: 'States',
        data: { id: validatedRequest.state_id },
      });
    }
    const response = await exotel(validatedRequest);

    await end({ ...response }, event);

    return res.ok(response);
  } catch (err) {
    await error({ error: err.toString() }, event);

    const statusCode = await assignHttpStatusCode(err);
    return res.status(statusCode).json({ error: err.toString() });
  }
};

module.exports = {
  config,
  create,
  patch,
  deleteTask,
  fetchOne,
  fetchAll,
  fetchAllMobile,
  sync,
  prepTaskReport,
  prepFormReport,
  download,
  patchTask,
  missedCall,
  taskCount,
  missedCallReport,
};
