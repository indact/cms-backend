class CleanupException extends Error {
  constructor(message, file) {
    super(message);
    this.file = file;
    this.name = this.constructor.name;
  }

  toString() {
    return {
      type: this.name,
      message: this.message,
      file: this.file,
    };
  }
}
module.exports = CleanupException;
