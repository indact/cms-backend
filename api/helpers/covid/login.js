const axios = require('axios');

const {
  BASE_URL,
  FORMIO_PORT,
  CONSTANTS: { formio },
} = sails.config.custom;
const baseURL = `${BASE_URL}:${FORMIO_PORT}`;

const { OperationException, UnauthorisedException } = require('../../exceptions');

module.exports = {
  friendlyName: 'Log in user',

  description: '',

  inputs: {
    logInBody: {
      type: 'ref',
      required: true,
    },
  },

  async fn(inputs, exits) {
    try {
      const {
        logInBody: { email, password },
      } = inputs;
      const userSubmission = await axios({
        method: 'POST',
        baseURL,
        url: formio.URL_COVID_LOGIN,
        headers: {
          'Content-Type': 'application/json',
        },
        data: {
          data: {
            email,
            password,
          },
        },
      });
      const {
        data: { _id: userId },
        headers: { 'x-jwt-token': FormioToken },
      } = userSubmission;

      const caller = await CovidCaller.findOne({ callerData: userId })
        .populate('languages')
        .populate('callerData')
        .populate('states');
      delete caller.callerData.created_at;
      delete caller.callerData.updated_at;
      delete caller.callerData.form;
      delete caller.callerData.data.password;
      const token = await sails.helpers.jwt.createToken(caller.id);
      return exits.success({ caller, token, FormioToken });
    } catch (err) {
      if (err.response && err.response.status === 401) {
        throw new UnauthorisedException.UnauthorisedException('Incorrect Login Credentials');
      }
      throw new OperationException.OperationException(err.message);
    }
  },
};
