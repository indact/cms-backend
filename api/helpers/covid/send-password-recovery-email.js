const nodemailer = require('nodemailer');
const { UnableToSendEmailException, RecordDoesNotExistException } = require('../../exceptions');

const { EMAIL_ID, EMAIL_PASS, CMS_FRONTEND_URL } = sails.config.custom;

module.exports = {
  friendlyName: 'Send password recovery email',
  inputs: {
    email: { type: 'string', required: true },
  },
  async fn({ email }) {
    try {
      const transporter = nodemailer.createTransport(
        `smtps://${EMAIL_ID}:${EMAIL_PASS}@smtp.gmail.com`,
      );
      const caller = await CovidCaller.findOne({ callerEmail: email });
      if (!caller) {
        throw new RecordDoesNotExistException.RecordDoesNotExistException('Caller');
      }
      const token = await sails.helpers.jwt.createToken(caller.id);
      const mailOptions = {
        from: `"Indus Action Tech Support" <${EMAIL_ID}>`,
        to: email,
        subject: 'Indus Action Covid Helpline Password Recovery',
        text: `Please click on the link below to reset your password:
        ${CMS_FRONTEND_URL}/covidhelpline/resetpassword?token=${token}`,
        html: `Please click on the link below to reset your password:<br>
        <a href="${CMS_FRONTEND_URL}/covidhelpline/resetpassword?token=${token}">Reset Password</a>`,
      };
      await transporter.sendMail(mailOptions);
    } catch (err) {
      switch (err.name) {
        case 'RecordDoesNotExistException':
          throw err;
        default:
          throw new UnableToSendEmailException(error.message, email);
      }
    }
  },
};
