/* eslint-disable no-tabs */
/* eslint-disable no-nested-ternary */
const { OperationException } = require('../../../exceptions');

module.exports = {
  friendlyName: '',
  description: '',
  inputs: {
    validatedRequest: {
      type: 'ref',
      required: true,
      description: 'The Assignee ID`s and task ID`s of the model making the request',
    },
  },
  exits: {
    success: {
      description: 'All done.',
    },
  },

  async fn(inputs, exits) {
    try {
      const { validatedRequest } = inputs;
      const { from, to } = validatedRequest;
      const state = validatedRequest.state_id;

      let q;
      const source = 'Exotel';

      // eslint-disable-next-line prefer-const
      q = `select sum((config->'exotelConfig'->>'missedCallCount')::integer) as totalMissedcall, count(mobile) as uniqueCalls
            from beneficiaries where deleted=false   
            ${source ? `and source= '${source}'` : ``}
            ${state ? `and state= '${state}'` : ``}
            ${
              from && to
                ? `and created_at  between '${from.toJSON()}' and '${to.toJSON()}'`
                : from
                ? `and created_at >= '${from.toJSON()}'`
                : to
                ? `and created_at <= '${to.toJSON()}'`
                : ``
}`;

      const result = (await sails.sendNativeQuery(q, [])).rows;

      const data = result[0];

      return exits.success({ message: 'Success', data });
    } catch (err) {
      switch (err.name) {
        default:
          throw new OperationException.OperationException(err.message);
      }
    }
  },
};
