/**
 * Archive.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'archive',
  attributes: {
    id: {
      type: 'string',
      required: true,
    },
    originalRecord: {
      type: 'ref',
      required: true,
      columnType: 'json',
    },
    originalRecordId: {
      type: 'ref',
      required: true,
      columnType: 'json',
    },
    fromModel: {
      type: 'string',
      columnType: 'text',
      required: true,
    },
  },
};
