/**
 * CovidBeneficiary.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: 'covid',
  tableName: 'beneficiaries',
  attributes: {
    id: { type: 'string', columnName: '_id' },
    beneficiaryName: { type: 'string', columnName: 'beneficiary_name' },
    beneficiaryPhone: { type: 'number', columnName: 'beneficiary_phone', unique: true },
    beneficiaryLocation: { type: 'string', columnName: 'beneficiary_location' },
    status: { type: 'ref' },
    callbackDate: { type: 'ref', columnType: 'datetime' },
    source: { type: 'string' },
    state: {
      model: 'covidState',
    },
    beneficiaryData: {
      model: 'submissions',
    },
    beneficiaryDataRecord: {
      collection: 'submissions',
      via: 'beneficiary',
    },
    caller: {
      model: 'covidCaller',
    },
  },
};
