/**
 * CovidCallers.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: 'covid',
  tableName: 'callers',
  attributes: {
    id: { type: 'string', columnName: '_id' },
    callerCity: { type: 'string', columnName: 'caller_city' },
    callerName: { type: 'string', columnName: 'caller_name' },
    callerEmail: { type: 'string', columnName: 'caller_email' },
    callerState: { model: 'covidState' },
    beneficiaries: {
      collection: 'covidBeneficiary',
      via: 'caller',
    },
    languages: {
      collection: 'covidLanguage',
      via: 'callers',
    },
    states: {
      collection: 'covidState',
      via: 'callers',
    },
    callerData: {
      model: 'covidCallerSubmission',
      unique: true,
    },
  },
  customToJSON() {
    return _.omit(this, ['created_at', 'updated_at']);
  },
};
