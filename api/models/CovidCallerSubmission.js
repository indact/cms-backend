/**
 * Submissions.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: 'mongoDB',
  tableName: 'submissions',
  attributes: {
    id: { type: 'string', columnName: '_id' },
    roles: {
      type: 'ref',
    },
    data: {
      type: 'ref',
    },
    callerBase: {
      collection: 'covidCaller',
      via: 'callerData',
    },
  },
  customToJSON() {
    return _.omit(this, [
      'created',
      'modified',
      'owner',
      'deleted',
      'access',
      'form',
      '__v',
      'externalIds',
      'metadata',
    ]);
  },
};
