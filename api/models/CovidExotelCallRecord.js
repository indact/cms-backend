/**
 * CovidExotelCallRecord.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: 'covid',
  tableName: 'exotelCallRecord',
  attributes: {
    id: { type: 'string', columnName: '_id' },
    sid: { type: 'string' },
    call: { type: 'ref' },
    status: { type: 'ref' },
    beneficiary: {
      model: 'covidBeneficiary',
    },
  },
  customToJSON() {
    return _.omit(this, ['created_at', 'updated_at']);
  },
};
