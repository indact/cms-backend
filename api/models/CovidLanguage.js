/**
 * CovidLanguages.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: 'covid',
  tableName: 'languages',
  attributes: {
    id: { type: 'string', columnName: '_id' },
    languageName: { type: 'string', columnName: 'language_name', unique: true },
    callers: {
      collection: 'covidCaller',
      via: 'languages',
    },
  },
  customToJSON() {
    return _.omit(this, ['created_at', 'updated_at']);
  },
};
