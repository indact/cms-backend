/**
 * CovidStates.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: 'covid',
  tableName: 'states',
  attributes: {
    id: { type: 'string', columnName: '_id' },
    stateName: { type: 'string', columnName: 'state_name', unique: true },

    beneficiaries: {
      collection: 'covidBeneficiary',
      via: 'state',
    },
    callers: {
      collection: 'covidCaller',
      via: 'states',
    },
  },
  customToJSON() {
    return _.omit(this, ['created_at', 'updated_at']);
  },
};
