const Joi = require('@hapi/joi');

const bulkImportSchema = Joi.object().keys({
  mobile: Joi.array()
    .min(1)
    .items(
      Joi.string()
        .regex(/^[0-9]+$/, 'numbers')
        .regex(/^[1-9]/, 'cannot start with zero')
        .length(10, 'utf8'),
    )
    .required(),

  state: Joi.string()
    .guid({ version: 'uuidv4' })
    .required(),
  tasks_status: Joi.string()
    .guid({ version: 'uuidv4' })
    .required(),
  task_type: Joi.string()
    .guid({ version: 'uuidv4' })
    .required(),
});

module.exports = {
  bulkImportSchema,
};
