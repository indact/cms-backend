const Joi = require('@hapi/joi');

const registerSchema = Joi.object().keys({
  name: Joi.string(),
  phone: Joi.string()
    .regex(/^[0-9]+$/, 'numbers')
    .regex(/^[1-9]/, 'cannot start with zero')
    .length(10, 'utf8'),
  email: Joi.string()
    .email()
    .required(),
  password: Joi.string()
    .trim()
    .min(6, 'utf8')
    .empty('')
    .required(),
  city: Joi.string(),
  languages: Joi.array().items(Joi.string()),
  state: Joi.string().regex(/^[a-f0-9]{24}$/, 'id'),
});

const loginSchema = Joi.object().keys({
  email: Joi.string()
    .email()
    .required(),
  password: Joi.string()
    .trim()
    .empty('')
    .required(),
});

const createLanguagesSchema = Joi.object().keys({
  languages: Joi.array().items(Joi.string()),
});

const importBeneficiarySchema = Joi.object().keys({
  state: Joi.string()
    .valid(
      'Andhra Pradesh',
      'Arunachal Pradesh',
      'Assam',
      'Bihar',
      'Chhattisgarh',
      'Delhi',
      'Goa',
      'Gujarat',
      'Haryana',
      'Himachal Pradesh',
      'Jammu and Kashmir',
      'Jharkhand',
      'Karnataka',
      'Kerala',
      'Madhya Pradesh',
      'Maharashtra',
      'Manipur',
      'Meghalaya',
      'Mizoram',
      'Nagaland',
      'Odisha',
      'Punjab',
      'Rajasthan',
      'Sikkim',
      'Tamil Nadu',
      'Telangana',
      'Tripura',
      'Uttar Pradesh',
      'Uttarakhand',
      'West Bengal',
    )
    .insensitive()
    .required(),
  beneficiaries: Joi.array().items(
    Joi.object().keys({
      phone: Joi.string()
        .regex(/^[0-9]+$/, 'numbers')
        .regex(/^[1-9]/, 'cannot start with zero')
        .length(10, 'utf8'),
      location: Joi.string(),
      source: Joi.string(),
      name: Joi.string(),
    }),
  ),
});

const getBeneficiariesSchema = Joi.object().keys({
  state: Joi.string().regex(/^[a-f0-9]{24}$/, 'id'),
  caller: Joi.string().regex(/^[a-f0-9]{24}$/, 'id'),
  unallocated: Joi.bool(),
});

const getAllocatedBeneficiariesSchema = Joi.object().keys({
  state: Joi.string().regex(/^[a-f0-9]{24}$/, 'id'),
});

const autoAllocateBeneficiariesSchema = Joi.object().keys({
  state: Joi.string().regex(/^[a-f0-9]{24}$/, 'id'),
  number: Joi.number().max(5),
});

const assignStatesToCallerSchema = Joi.object().keys({
  states: Joi.array().items(
    Joi.string()
      .valid(
        'Andhra Pradesh',
        'Arunachal Pradesh',
        'Assam',
        'Bihar',
        'Chhattisgarh',
        'Delhi',
        'Goa',
        'Gujarat',
        'Haryana',
        'Himachal Pradesh',
        'Jammu and Kashmir',
        'Jharkhand',
        'Karnataka',
        'Kerala',
        'Madhya Pradesh',
        'Maharashtra',
        'Manipur',
        'Meghalaya',
        'Mizoram',
        'Nagaland',
        'Odisha',
        'Punjab',
        'Rajasthan',
        'Sikkim',
        'Tamil Nadu',
        'Telangana',
        'Tripura',
        'Uttar Pradesh',
        'Uttarakhand',
        'West Bengal',
      )
      .insensitive()
      .required(),
  ),
  caller: Joi.string().regex(/^[a-f0-9]{24}$/, 'id'),
});

const updateBeneficiarySchema = Joi.object().keys({
  submissionId: Joi.string().regex(/^[a-f0-9]{24}$/, 'id'),
  beneficiaryId: Joi.string().regex(/^[a-f0-9]{24}$/, 'id'),
  callbackDate: Joi.date().timestamp('javascript'),
  status: Joi.object().keys({
    label: Joi.string(),
    value: Joi.string(),
  }),
});

const getSubmissionSchema = Joi.object().keys({
  id: Joi.string().regex(/^[a-f0-9]{24}$/, 'id'),
});

const getCallersSchema = Joi.object().keys({
  name: Joi.string(),
  email: Joi.string(),
});

const bulkRegisterSchema = Joi.object().keys({
  users: Joi.array().items(
    Joi.object().keys({
      name: Joi.string(),
      phone: Joi.string()
        .regex(/^[0-9]+$/, 'numbers')
        .regex(/^[1-9]/, 'cannot start with zero')
        .length(10, 'utf8'),
      email: Joi.string()
        .email()
        .required(),
      city: Joi.string(),
      languages: Joi.array().items(Joi.string()),
    }),
  ),
});

const getStatusReportSchema = Joi.object().keys({
  states: Joi.array().items(
    Joi.string()
      .valid(
        'Andhra Pradesh',
        'Arunachal Pradesh',
        'Assam',
        'Bihar',
        'Chhattisgarh',
        'Delhi',
        'Goa',
        'Gujarat',
        'Haryana',
        'Himachal Pradesh',
        'Jammu and Kashmir',
        'Jharkhand',
        'Karnataka',
        'Kerala',
        'Madhya Pradesh',
        'Maharashtra',
        'Manipur',
        'Meghalaya',
        'Mizoram',
        'Nagaland',
        'Odisha',
        'Punjab',
        'Rajasthan',
        'Sikkim',
        'Tamil Nadu',
        'Telangana',
        'Tripura',
        'Uttar Pradesh',
        'Uttarakhand',
        'West Bengal',
      )
      .insensitive(),
  ),
  callers: Joi.array().items(Joi.string().email()),
});

const sendPasswordRecoveryEmailSchema = Joi.object().keys({
  email: Joi.string()
    .email()
    .required(),
});

const resetPasswordSchema = Joi.object().keys({
  password: Joi.string()
    .trim()
    .empty('')
    .required(),
});

module.exports = {
  registerSchema,
  loginSchema,
  createLanguagesSchema,
  importBeneficiarySchema,
  getBeneficiariesSchema,
  getAllocatedBeneficiariesSchema,
  autoAllocateBeneficiariesSchema,
  assignStatesToCallerSchema,
  updateBeneficiarySchema,
  getSubmissionSchema,
  getCallersSchema,
  bulkRegisterSchema,
  getStatusReportSchema,
  sendPasswordRecoveryEmailSchema,
  resetPasswordSchema,
};
