const Joi = require('@hapi/joi');

const dataByBeneficiaryCategorySchema = Joi.object().keys({
  email: Joi.string().email(),
  category: Joi.string()
    .valid('dgCwsn', 'dg', 'ews', 'bpl', 'weakerSection', 'dgSpecial')
    .insensitive(),
  stage: Joi.string()
    .valid('Identification', 'Application', 'Lottery', 'Admission', 'Retention')
    .insensitive(),
  state: Joi.string()
    .valid(
      'Andhra Pradesh',
      'Arunachal Pradesh',
      'Assam',
      'Bihar',
      'Chhattisgarh',
      'Delhi',
      'Goa',
      'Gujarat',
      'Haryana',
      'Himachal Pradesh',
      'Jammu and Kashmir',
      'Jharkhand',
      'Karnataka',
      'Kerala',
      'Madhya Pradesh',
      'Maharashtra',
      'Manipur',
      'Meghalaya',
      'Mizoram',
      'Nagaland',
      'Odisha',
      'Punjab',
      'Rajasthan',
      'Sikkim',
      'Tamil Nadu',
      'Telangana',
      'Tripura',
      'Uttar Pradesh',
      'Uttarakhand',
      'West Bengal',
    )
    .insensitive()
    .required(),
  complete: Joi.bool(),
});

const dataByTaskStatusSchema = Joi.object().keys({
  email: Joi.string().email(),
  status: Joi.string()
    .valid(
      'Call Not Done',
      'Call Done',
      'Ineligible',
      'Call Again',
      'Escalated',
      'Repeated',
      'Finished',
      'Stopped',
      'Swtiched Off',
      'Not Responding',
      'Wrong Number',
    )
    .insensitive(),
  stage: Joi.string()
    .valid('Identification', 'Application', 'Lottery', 'Admission', 'Retention')
    .insensitive(),
  stage_till: Joi.string()
    .valid('Identification', 'Application', 'Lottery', 'Admission', 'Retention')
    .insensitive(),
  state: Joi.string()
    .valid(
      'Andhra Pradesh',
      'Arunachal Pradesh',
      'Assam',
      'Bihar',
      'Chhattisgarh',
      'Delhi',
      'Goa',
      'Gujarat',
      'Haryana',
      'Himachal Pradesh',
      'Jammu and Kashmir',
      'Jharkhand',
      'Karnataka',
      'Kerala',
      'Madhya Pradesh',
      'Maharashtra',
      'Manipur',
      'Meghalaya',
      'Mizoram',
      'Nagaland',
      'Odisha',
      'Punjab',
      'Rajasthan',
      'Sikkim',
      'Tamil Nadu',
      'Telangana',
      'Tripura',
      'Uttar Pradesh',
      'Uttarakhand',
      'West Bengal',
    )
    .insensitive()
    .required(),
  complete: Joi.bool(),
});
module.exports = {
  dataByBeneficiaryCategorySchema,
  dataByTaskStatusSchema,
};
