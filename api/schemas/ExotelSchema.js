const Joi = require('@hapi/joi');

const callMaskingSchema = Joi.object().keys({
  beneficiary: Joi.string().regex(/^[a-f0-9]{24}$/, 'id'),
});

module.exports = {
  callMaskingSchema,
};
