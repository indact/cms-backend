/* eslint-disable no-param-reassign */
/* eslint-disable no-await-in-loop */
const axios = require('axios');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const camelCase = require('lodash.camelcase');
const nodemailer = require('nodemailer');
const fs = require('fs');
const { UnableToSendEmailException } = require('../exceptions');

const {
  EMAIL_ID,
  EMAIL_PASS,
  BASE_URL,
  CMS_PORT,
  FORMIO_PORT,
  CONSTANTS: { formio },
} = sails.config.custom;

const formioBaseURL = `${BASE_URL}:${FORMIO_PORT}`;

const flatten = function (data) {
  let i;
  let l;
  const result = {};
  function recurse(cur, prop) {
    if (Object(cur) !== cur) {
      result[prop] = cur;
    } else if (Array.isArray(cur)) {
      for (i = 0, l = cur.length; i < l; i += 1) {
        recurse(cur[i], `${prop}[${i}]`);
      }
      if (l === 0) {
        result[prop] = [];
      }
    } else if (cur instanceof Date) {
      result[prop] = cur.toDateString();
    } else {
      let isEmpty = true;
      // eslint-disable-next-line guard-for-in
      for (const p in cur) {
        isEmpty = false;
        // console.log(prop, p, cur[p] instanceof Date);
        recurse(cur[p], prop ? `${prop}.${p}` : p);
      }
      if (isEmpty && prop) {
        result[prop] = {};
        // console.log(prop, cur[prop]);
      }
    }
  }
  recurse(data, '');
  return result;
};

const getHeaderFromSchema = function (schema, header = []) {
  try {
    const { components } = schema;
    for (const {
      input, key, label, columns, ...rest
    } of components) {
      let required;
      if ('validate' in rest) {
        ({ required } = rest.validate);
      }
      // eslint-disable-next-line no-continue
      if (label === 'Submit') continue;
      if (input) {
        header.push({ id: key, title: required ? `(Required) ${label}` : label });
      } else {
        columns.forEach((column) => {
          getHeaderFromSchema(column, header);
        });
      }
    }
    return header;
  } catch (error) {
    throw error;
  }
};

const sendMail = async function (message) {
  try {
    // create reusable transporter object using the default SMTP transport
    const transporter = nodemailer.createTransport(
      `smtps://${EMAIL_ID}:${EMAIL_PASS}@smtp.gmail.com`,
    );

    // setup e-mail data with unicode symbols
    const mailOptions = {
      from: `"Indus Action Tech Support" <${EMAIL_ID}>`, // sender email
      ...message,
    };

    // send mail with defined transport object
    await transporter.sendMail(mailOptions);
  } catch (error) {
    throw new UnableToSendEmailException(error.message, message.to);
  }
};

const prepTasksReport = async function prepareReportForDownload(
  {
    data: {
      end = Date.now(),
      start = end - 7 * 24 * 60 * 60 * 1000,
      filter = {},
      user,
      email = user,
    },
  },
  cb,
) {
  try {
    let filterQuery = '';
    const header = [
      { id: 'created_at', title: 'Created At' },
      { id: 'updated_at', title: 'Updated At' },
      { id: 'task_type_name', title: 'Task Type' },
      { id: 'task_status_name', title: 'Task Status' },
      { id: 'assignee_mobile', title: 'Assignee Mobile' },
      { id: 'assignee_email', title: 'Assignee Email' },
      { id: 'beneficiary_mobile', title: 'Beneficiary Moblie' },
      { id: 'state_name', title: 'State' },
    ];
    const timeStamp = Math.floor(new Date() / 1000);

    let directory = './reports';
    if (!fs.existsSync(directory)) {
      fs.mkdirSync(directory);
    }
    directory = './reports/task';
    if (!fs.existsSync(directory)) {
      fs.mkdirSync(directory);
    }

    fileName = `${user}_${timeStamp}`;
    const path = `./reports/task/${fileName}.csv`;

    const csvWriter = await createCsvWriter({
      path,
      header,
    });

    for (const [key, value] of Object.entries(filter)) {
      filterQuery += `${key}_id = '${value}' AND `;
    }

    const query = `
      SELECT created_at, updated_at, task_type_name, 
      task_status_name, assignee_mobile, assignee_email, 
      beneficiary_mobile, state_name
      FROM tasks_view
      WHERE 
        ${filterQuery} 
        ((ROUND(EXTRACT(EPOCH FROM updated_at)::numeric, 5)*1000) < ${new Date(end).getTime()} 
        AND (ROUND(EXTRACT(EPOCH FROM updated_at)::numeric, 5)*1000) > ${new Date(
          start,
  ).getTime()});
    `;
    const tasks = (await sails.sendNativeQuery(query, [])).rows;

    await csvWriter.writeRecords(tasks).then(async () => {
      message = {
        to: email,
        subject: 'Tasks Report Download',
        text: `Hi,
        Your requested tasks report is ready for download. Click the link below.
        ${BASE_URL}:${CMS_PORT}/tasks/download?task=${fileName}`,
      };
      await sendMail(message);
    });

    return cb();
  } catch (error) {
    sails.log.error(error);
    return 0;
  }
};

const prepFormReport = async function prepareFormReportForDownload(
  {
    data: {
      end = Date.now(),
      start = end - 7 * 24 * 60 * 60 * 1000,
      filter: { form, owner = '' },
      user,
      email = user,
    },
  },
  cb,
) {
  try {
    schema = await Forms.findOne({ id: form, deleted: null });

    if (!schema) {
      message = {
        to: email,
        subject: 'Form Report Download',
        text: `Hi,
        Your requested form report cannot be prepared as form with id ${form} does not exist`,
      };
      await sendMail(message);
      return cb();
    }

    header = await getHeaderFromSchema(schema);

    const timeStamp = Math.floor(new Date() / 1000);

    let directory = './reports';
    if (!fs.existsSync(directory)) {
      fs.mkdirSync(directory);
    }
    directory = './reports/form';
    if (!fs.existsSync(directory)) {
      fs.mkdirSync(directory);
    }

    fileName = `${user}_${timeStamp}`;
    const path = `./reports/form/${fileName}.csv`;
    const csvWriter = await createCsvWriter({
      path,
      header,
    });
    const data = (
      await Submissions.find({
        owner: { contains: owner },
        form,
        updated_at: { '>': new Date(start), '<': new Date(end) },
      })
    ).map(submission => submission.data);
    await csvWriter.writeRecords(data).then(async () => {
      message = {
        to: email,
        subject: 'Form Report Download',
        text: `Hi,
        Your requested form report is ready for download. Click the link below.
        ${BASE_URL}:${CMS_PORT}/tasks/download?form=${fileName}`,
      };
      await sendMail(message);
    });

    return cb();
  } catch (error) {
    sails.log.error(error);
    return 0;
  }
};

const bulkRegister = async function ({ data: { users } }, cb) {
  try {
    const errors = [];
    for (let i = 0; i < users.length; i += 1) {
      const {
        name, email, phone, city, languages, state,
      } = users[i];
      try {
        const password = Math.random()
          .toString(36)
          .substring(7);
        const userSubmission = await axios({
          method: 'POST',
          baseURL: formioBaseURL,
          url: formio.URL_COVID_VOLUNTEER_REGISTRATION,
          headers: {
            'Content-Type': 'application/json',
          },
          data: {
            data: {
              name,
              email,
              phoneNumber: phone,
              password,
            },
          },
        });
        const {
          data: { _id: userId },
        } = userSubmission;
        const { id } = await CovidCaller.create({
          callerName: name,
          callerEmail: email,
          callerCity: city,
          callerData: userId,
          callerState: state,
        }).fetch();
        const languageIds = (
          await CovidLanguage.find({
            languageName: { in: languages },
          })
        ).map(language => language.id);
        await CovidCaller.addToCollection(id, 'languages', languageIds);
        message = {
          to: email,
          subject: 'Indus Action Covid Helpline Registration',
          text: `You have been registered to the Covid Helpline.
          Your password is ${password}.
          Log in at https://indusactioncms.com/covidhelpline`,
        };
        await sendMail(message);
      } catch (err) {
        errors.push({
          email,
          error: [err.message],
        });
      }
    }
    message = {
      to: `cecil@indusaction.org`,
      subject: 'Bulk Registration Report',
      text: `Bulk Registration has been completed with ${errors.length} errors.user
      Errors: ${errors.map(x => JSON.stringify(x))}`,
    };
    await sendMail(message);
    return cb();
  } catch (error) {
    sails.log.error(error);
    return 0;
  }
};

const prepBeneficiaryReport = async function prepareBeneficiaryReport(
  {
    data: {
      state, category, stage, complete, user, email = user,
    },
  },
  cb,
) {
  try {
    sails.log.info('Task Started');
    let query = {};
    let stageList = [];

    const stateId = (
      await States.findOne({
        name: state,
      })
    ).id;

    const stateName = camelCase(state);

    const pForm = await Forms.findOne({
      name: `${stateName}-persistent`,
      deleted: null,
    });

    if (!pForm) throw new Error(`No persistent form found for ${state}`);

    query.form = pForm.id;

    if (category) query['data.category'] = category;

    const persistentSubmissions = await Submissions.find({ ...query }).meta({
      enableExperimentalDeepTargets: true,
    });

    let beneficiaries = [];
    let beneficiaryIds = [];

    if (persistentSubmissions.length > 0) {
      // submissionsId's are required to search for the connected beneficiaries
      let submissionIds = persistentSubmissions.map(submission => submission.id);
      // persistentSubmissions = persistentSubmissions.map(submission => submission.data);

      /** if complete flag is set to true,
       * then only beneficiaries that has all data up to the specified stage shall be returned
       * else all beneficiaryies shall be returned, with data util that stage
       */
      if (stage && complete) {
        query = `
          SELECT * FROM tasks
          WHERE type IN (SELECT id FROM task_type 
            WHERE name = '${stage}' AND state = '${stateId}')`;
        beneficiaryIds = (await sails.sendNativeQuery(query, [])).rows
          .filter(row => row.submission_id)
          .map(row => row.beneficiary);
      }

      query = `
        SELECT b.id AS id, mobile AS mobile, config->>'pSubmissionId' AS pSubmissionId,
          name AS state
        FROM beneficiaries b
        INNER JOIN states s ON b.state = s.id
        WHERE config->>'pSubmissionId' IN ('${submissionIds.join('\', \'')}')
        ${beneficiaryIds.length > 0 ? `AND b.id IN ('${beneficiaryIds.join('\', \'')}')` : ''}`;

      beneficiaries = (await sails.sendNativeQuery(query, [])).rows;

      beneficiaries = beneficiaries.map((beneficiary) => {
        beneficiary.persistent = persistentSubmissions.filter(
          submission => submission.id === beneficiary.psubmissionid,
        )[0].data;
        delete beneficiary.psubmissionid;
        return beneficiary;
      });

      beneficiaryIds = beneficiaries.map(beneficiary => beneficiary.id);

      if (beneficiaryIds.length > 0) {
        // Get a list of task types uptil the mentioned stage
        query = `SELECT * FROM task_type WHERE state = '${stateId}'
          ${
            stage
              ? `AND sequence <= (SELECT sequence FROM task_type 
            WHERE name = '${stage}' AND state = '${stateId}')`
              : ''
}
          ORDER BY sequence asc;`;

        stageList = (await sails.sendNativeQuery(query, [])).rows.map(row => row.name);

        for (let i = 0; i < stageList.length; i += 1) {
          sails.log.info(`Task Data :: ${stageList[i]}`);
          query = `SELECT submission_id AS id, beneficiary FROM tasks 
            WHERE type IN (SELECT id FROM task_type 
              WHERE name = '${stageList[i]}' 
              AND state = '${stateId}')
            AND beneficiary IN ('${beneficiaryIds.join('\', \'')}');`;

          const tasks = (await sails.sendNativeQuery(query, [])).rows.filter(
            submission => submission.id,
          );

          submissionIds = tasks.map(submission => submission.id).filter(id => id);

          const stageData = await Submissions.find({ id: { in: submissionIds } });

          beneficiaries = beneficiaries.map((beneficiary) => {
            // get submission id mapped to this beneficiary id
            let sId = tasks.filter(task => task.beneficiary === beneficiary.id)[0];
            if (sId) {
              sId = sId.id;
              const beneficiaryStageData = stageData.filter(submission => submission.id === sId)[0];
              if (beneficiaryStageData) {
                beneficiary[stageList[i]] = beneficiaryStageData.data;
              }
            }
            return beneficiary;
          });
        }
      }
    }

    // Prepare the header for the CSV
    // First get the forms
    // Use the function to get headers from single forms and push them to the header array
    sails.log.info('Preparing CSV Headers');
    const header = [
      { id: 'mobile', title: 'Mobile' },
      { id: 'state', title: 'State' },
    ];
    sails.log.info('CSV Header :: Persistent');
    header.push(
      ...(await getHeaderFromSchema(pForm)).map((h) => {
        h.id = `persistent.${h.id}`;
        h.title = `Persistent :: ${h.title}`;
        return h;
      }),
    );
    for (let i = 0; i < stageList.length; i += 1) {
      sails.log.info(`CSV Header :: ${stageList[i]}`);
      const formName = `${stateName}-${stageList[i].toLowerCase()}`;
      const form = await Forms.findOne({
        name: formName,
        deleted: null,
      });
      // sails.log.debug(form, formName);
      if (form) {
        header.push(
          ...(await getHeaderFromSchema(form)).map((h) => {
            h.id = `${stageList[i]}.${h.id}`;
            h.title = `${stageList[i]} :: ${h.title}`;
            return h;
          }),
        );
      }
    }

    // flatten the data
    sails.log.info('Flattening Data');
    const data = beneficiaries.map(beneficiary => flatten(beneficiary));

    let directory = './reports';
    if (!fs.existsSync(directory)) {
      fs.mkdirSync(directory);
    }
    directory = './reports/beneficiary';
    if (!fs.existsSync(directory)) {
      fs.mkdirSync(directory);
    }

    const timeStamp = Math.floor(new Date() / 1000);

    fileName = `${user}_${timeStamp}`;
    const path = `./reports/beneficiary/${fileName}.csv`;
    const csvWriter = await createCsvWriter({
      path,
      header,
    });

    sails.log.info('Writing to CSV file');
    // console.time();
    await csvWriter.writeRecords(data).then(async () => {
      message = {
        to: email,
        subject: 'Form Report Download',
        text: `Hi,
        Your requested beneficiary report is ready for download. Click the link below.
        ${BASE_URL}:${CMS_PORT}/tasks/download?beneficiary=${fileName}`,
      };
      await sendMail(message);
    });
    sails.log.info('Writing Complete and Email Sent');
    // console.timeEnd();
    return cb();
  } catch (error) {
    // console.log(error);
    return 0;
  }
};

const prepTaskReportByStatus = async function prepareBeneficiaryReport(
  {
    data: {
      state, status, stage, stage_till: stageTill, complete, user, email = user,
    },
  },
  cb,
) {
  const stateId = state && {
    id: (await States.findOne({ name: state })).id,
    key: 'state',
  };
  const statusId = status && {
    id: (await Tasks_status.findOne({ name: status, state: stateId.id })).id,
    key: 'status',
  };

  const stateName = camelCase(state);

  const pForm = await Forms.findOne({
    name: `${stateName}-persistent`,
    deleted: null,
  });

  if (!pForm) throw new Error(`No persistent form found for ${state}`);

  /* eslint-disable no-nested-ternary */
  let query = `SELECT * FROM task_type WHERE state = '${stateId.id}'
    ${
      stageTill
        ? `AND sequence <= (SELECT sequence FROM task_type 
          WHERE name = '${stageTill}' AND state = '${stateId.id}')`
        : stage
        ? `AND name = '${stage}'`
        : ''
}
      ORDER BY sequence asc;`;
  /* eslint-enable no-nested-ternary */

  const submissions = {};
  const stageList = (await sails.sendNativeQuery(query, [])).rows.map((row) => {
    submissions[row.name.toLowerCase()] = [];
    return { id: row.id, name: row.name };
  });

  query = '';

  [stateId, statusId].forEach((item) => {
    if (item) {
      query += `\nAND t.${item.key} = '${item.id}'`;
    }
  });

  query = `SELECT b.mobile, st.name AS state, 
  s.name AS status, b.config::json->>'pSubmissionId' AS psubmissionid, 
  u.name AS assigneeName, u.mobile AS assigneeMobile,
  tt.name AS stage, t.submission_id AS submissionId
  FROM tasks t
  INNER JOIN users u on u.id = t.assignee 
  INNER JOIN beneficiaries b ON b.id = t.beneficiary
  INNER JOIN tasks_status s ON s.id = t.status
  INNER JOIN states st ON st.id = t.state
  INNER JOIN task_type tt ON tt.id = t.type 
  WHERE type in ('${stageList.map(type => type.id).join('\',\'')}')${query}
  ORDER BY b.mobile;`;

  const taskList = (await sails.sendNativeQuery(query, [])).rows;
  const beneficiaries = {};
  for (let i = 0; i < taskList.length; i += 1) {
    const task = taskList[i];
    const {
      mobile,
      assigneename: assigneeName,
      assigneemobile: assigneeMobile,
      submissionid,
      psubmissionid,
    } = task;
    submissions[task.stage.toLowerCase()].push({ mobile, id: submissionid });
    let data = beneficiaries[task.mobile];
    if (!data) {
      data = {
        mobile,
        state: task.state,
        persistent: { id: psubmissionid },
        [task.stage.toLowerCase()]: {
          id: submissionid,
          status: task.status,
          assigneeMobile,
          assigneeName,
        },
      };
      beneficiaries[task.mobile] = { ...data };
    } else {
      data[task.stage.toLowerCase()] = {
        id: submissionid,
        status: task.status,
        assigneeMobile,
        assigneeName,
      };
      beneficiaries[task.mobile] = { ...data };
    }
  }

  const pSubmissions = Object.values(beneficiaries).map(beneficiary => ({
    mobile: beneficiary.mobile,
    id: beneficiary.persistent.id,
  }));

  sails.log.info('Preparing CSV Headers');
  const header = [
    { id: 'mobile', title: 'Mobile' },
    { id: 'state', title: 'State' },
  ];
  sails.log.info('Preparing CSV Header :: Persistent');
  header.push(
    ...(await getHeaderFromSchema(pForm)).map((h) => {
      h.id = `persistent.data.${h.id}`;
      h.title = `Persistent data :: ${h.title}`;
      return h;
    }),
  );

  // get the pSubmission Data
  query = {};
  query.form = pForm.id;
  query.id = { in: pSubmissions.map(pSubmission => pSubmission.id).filter(id => id) };

  const persistentSubmissions = await Submissions.find({ ...query });
  sails.log.info('Getting Persistent Submission Data');
  persistentSubmissions.map((submission) => {
    const [submissionMap] = pSubmissions.filter(pSubmission => pSubmission.id === submission.id);
    const { mobile } = submissionMap;
    beneficiaries[mobile].persistent = { data: submission.data };
    return 0;
  });

  for (let i = 0; i < Object.keys(submissions).length; i += 1) {
    const currentStage = Object.keys(submissions)[i];
    header.push(
      {
        id: `${currentStage}.status`,
        title: `${currentStage} :: status`,
      },
      {
        id: `${currentStage}.assigneeName`,
        title: `${currentStage} :: assigneeName`,
      },
      {
        id: `${currentStage}.assigneeMobile`,
        title: `${currentStage} :: assigneeMobile`,
      },
    );
    const formName = `${stateName}-${currentStage.toLowerCase()}`;
    const form = await Forms.findOne({
      name: formName,
      deleted: null,
    });
    if (form) {
      sails.log.info(`Preparing CSV Header :: ${currentStage}`);
      header.push(
        {
          id: `${currentStage}.data.created`,
          title: `${currentStage} :: Created At`,
        },
        {
          id: `${currentStage}.data.modified`,
          title: `${currentStage} :: Modified At`,
        },
        ...(await getHeaderFromSchema(form)).map((h) => {
          h.id = `${currentStage}.data.${h.id}`;
          h.title = `${currentStage} data :: ${h.title}`;
          return h;
        }),
      );

      // Get the submission data
      sails.log.info(`Getting ${currentStage} Submission Data`);
      query = {};
      query.form = form.id;
      query.id = {
        in: submissions[currentStage].map(submission => submission.id).filter(id => id),
      };

      const stageSubmissions = await Submissions.find({ ...query });
      // console.log(stageSubmissions[0].created_at.toLocaleString());
      stageSubmissions.forEach((stageSubmission) => {
        const [submissionMap] = submissions[currentStage].filter(
          submission => submission.id === stageSubmission.id,
        );
        const { mobile } = submissionMap;
        beneficiaries[mobile][currentStage.toLowerCase()].data = {
          created: stageSubmission.created_at.toLocaleString(),
          modified: stageSubmission.updated_at.toLocaleString(),
          ...stageSubmission.data,
        };
      });
    }
  }

  // flatten the data
  sails.log.info('Flattening Data');
  const data = Object.values(beneficiaries).map(beneficiary => flatten(beneficiary));

  let directory = './reports';
  if (!fs.existsSync(directory)) {
    fs.mkdirSync(directory);
  }
  directory = './reports/beneficiary';
  if (!fs.existsSync(directory)) {
    fs.mkdirSync(directory);
  }

  const timeStamp = Math.floor(new Date() / 1000);

  fileName = `${user}_${timeStamp}`;
  const path = `./reports/beneficiary/${fileName}.csv`;
  const csvWriter = await createCsvWriter({
    path,
    header,
  });

  sails.log.info('Writing to CSV file');

  await csvWriter.writeRecords(data).then(async () => {
    message = {
      to: email,
      subject: 'Form Report Download',
      text: `Hi,
      Your requested beneficiary report is ready for download. Click the link below.
      ${BASE_URL}:${CMS_PORT}/tasks/download?beneficiary=${fileName}`,
    };
    await sendMail(message);
  });
  sails.log.info('Writing Complete and Email Sent');

  return cb();
};
exports._processors = {
  prepTasksReport,
  prepFormReport,
  prepBeneficiaryReport,
  prepTaskReportByStatus,
  bulkRegister,
};
