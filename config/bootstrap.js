/* eslint-disable no-loop-func */
const kue = require('kue-scheduler');

const paths = ['./reports/task', './reports/form', './reports/beneficiary'];
const fs = require('fs');
const { CleanupException } = require('../api/exceptions');

let config;
if (!process.env.NODE_ENV) {
  config = require('../config.dev');
}

exports.bootstrap = function (cb) {
  // Scheduler Options
  const options = {
    prefix: 'w',
    skipConfig: false,
    redis: {
      port: process.env.REDIS_PORT || config.REDIS_PORT,
      host: process.env.REDIS_HOST || config.REDIS_HOST,
      db: 2,
    },
    restore: true,
  };
  // Create the queue object
  const Queue = kue.createQueue(options);

  // perform cleanup
  Queue.remove(
    {
      unique: 'cleanup',
    },
    (error, response) => {
      sails.log.info(response);
    },
  );

  // processing jobs
  Queue.process('cleanup', (job, done) => {
    // put in try catch block
    try {
      sails.log.info('Processing job with id %s at %s', job.id, new Date().toString());
      currentTime = Math.floor(new Date() / 1000);
      // loop through the paths
      paths.forEach((path) => {
        // Check if path exist
        if (fs.existsSync(path)) {
          // Loop through the files in the folder
          fs.readdirSync(path).forEach((file) => {
            if (file) {
              // try catch block so that the process continues
              // without interruption in case of an error
              try {
                // Get time of the file creation
                time = Number(file.split('_')[1].split('.')[0]);
                // Check if file is more than 24 hours old
                if (currentTime - time > 24 * 60 * 60) {
                  // delete the file
                  fs.unlink(`${path}/${file}`, (err) => {
                    // check if there is a deletion error
                    if (err) throw new CleanupException(err.message, `${path}/${file}`);
                    // if there are no error, then write to success log
                    fs.appendFile(
                      './reports/success.log',
                      `${new Date(currentTime * 1000).toString()}:: Deleted ${path}/${file}\n`,
                      (err2) => {
                        // in case of a logging error print error to terminal
                        if (err2) sails.log.error(`Logging Error:: ${err2}`);
                      },
                    );
                  });
                }
              } catch (error) {
                // if a deletion error occurs, write to fail log
                fs.appendFile(
                  './reports/fail.log',
                  `${new Date(currentTime * 1000).toString()}:: Deletion failed task/${
                    error.file
                  } with ${error.toString()}\n`,
                  (err2) => {
                    // in case of logging error print error to terminal
                    sails.log.error(`logging error:: ${err2}`);
                  },
                );
                sails.log.error(error.message);
              }
            }
          });
        }
      });
      return done(null, { deliveredAt: new Date() });
    } catch (error) {
      sails.log.error(error);
      return done(null, { deliveredAt: new Date() });
    }
  });

  // listen on scheduler errors
  Queue.on('schedule error', (error) => {
    // handle all scheduling errors here
    sails.log.error(error);
  });

  // listen on success scheduling
  Queue.on('schedule success', (job) => {
    // a highly recommended place to attach
    // job instance level events listeners
    job
      .on('complete', (result) => {
        sails.log.info('Job completed with data ', result); // write to success log
      })
      .on('failed attempt', (errorMessage, doneAttempts) => {
        sails.log.warn(`Job failed ${doneAttempts} time(s) with ${errorMessage}`); // do nothing
      })
      .on('failed', (errorMessage) => {
        sails.log.error(`Job failed with ${errorMessage}`); // write to fail log
      })
      .on('progress', (progress, data) => {
        sails.log.info(`\r  job #${job.id} ${progress}% complete with data `, data); // do nothing
      });
  });

  // prepare a job to perform
  // dont save it
  const job = Queue.createJob('cleanup', { to: 'any' })
    .attempts(3)
    .backoff({ delay: 60000, type: 'fixed' })
    .priority('normal');
  // .unique('cleanup');

  // schedule a job then
  Queue.every('0 0 * * *', job);
  // Queue.schedule('2 seconds from now', job);
  cb();
};
