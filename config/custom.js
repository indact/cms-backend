/**
 * Custom configuration
 * (sails.config.custom)
 *
 * One-off settings specific to your application.
 *
 * For more information on custom configuration, visit:
 * https://sailsjs.com/config/custom
 */
const CONSTANTS = require('../api/constants');

let config;
if (!process.env.NODE_ENV) {
  config = require('../config.dev');
}

module.exports.custom = {
  /** *************************************************************************
   *                                                                          *
   * Any other custom config this Sails app should use during development.    *
   *                                                                          *
   ************************************************************************** */
  // mailgunDomain: 'transactional-mail.example.com',
  // mailgunSecret: 'key-testkeyb183848139913858e8abd9a3',
  // stripeSecret: 'sk_test_Zzd814nldl91104qor5911gjald',
  // …
  CONSTANTS,
  EMAIL_ID: process.env.EMAIL_ID || config.EMAIL_ID,
  EMAIL_PASS: process.env.EMAIL_PASS || config.EMAIL_PASS,
  SENDER_ID_INF: 'CMSINF',
  SENDER_ID_OTP: 'CMSOTP',
  ROUTE_NO: '4',
  JWT_SECRET_KEY: process.env.JWT_SECRET_KEY || config.JWT_SECRET_KEY,
  SMS_SECRET_KEY: process.env.SMS_SECRET_KEY || config.SMS_SECRET_KEY,
  BASE_URL: process.env.BASE_URL || config.BASE_URL,
  CMS_PORT: process.env.CMS_PORT || config.CMS_PORT,
  FORMIO_PORT: process.env.FORMIO_PORT || config.FORMIO_PORT,
  FORMIO_EMAIL: process.env.FORMIO_EMAIL || config.FORMIO_EMAIL,
  FORMIO_PASSWORD: process.env.FORMIO_PASSWORD || config.FORMIO_PASSWORD,
  EXOTEL_API_KEY: process.env.EXOTEL_API_KEY || config.EXOTEL_API_KEY,
  EXOTEL_API_TOKEN: process.env.EXOTEL_API_TOKEN || config.EXOTEL_API_TOKEN,
  EXOTEL_ACCOUNT_SID: process.env.EXOTEL_ACCOUNT_SID || config.EXOTEL_ACCOUNT_SID,
  EXOTEL_SUBDOMAIN: process.env.EXOTEL_SUBDOMAIN || config.EXOTEL_SUBDOMAIN,
  EXOPHONE_NUMBER: {
    delhi: process.env.EXOPHONE_DELHI || config.EXOPHONE_DELHI,
    karnataka: process.env.EXOPHONE_KARNATAKA || config.EXOPHONE_KARNATAKA,
    maharashtra: process.env.EXOPHONE_MAHARASHTRA || config.EXOPHONE_MAHARASHTRA,
    gujarat: process.env.EXOPHONE_GUJARAT || config.EXOPHONE_GUJARAT,
    madhyaPradesh: process.env.EXOPHONE_MADHYA_PRADESH || config.EXOPHONE_MADHYA_PRADESH,
    rajasthan: process.env.EXOPHONE_RAJASTHAN || config.EXOPHONE_RAJASTHAN,
    uttarPradesh: process.env.EXOPHONE_UTTAR_PRADESH || config.EXOPHONE_UTTAR_PRADESH,
    chhatisgarh: process.env.EXOPHONE_CHHATISGARH || config.EXOPHONE_CHHATISGARH,
    bihar: process.env.EXOPHONE_BIHAR || config.EXOPHONE_BIHAR,
  },
  CMS_BASE_URL: process.env.CMS_BASE_URL || config.CMS_BASE_URL,
  CMS_FRONTEND_URL: process.env.CMS_FRONTEND_URL || config.CMS_FRONTEND_URL,
  FORMIO_URL: process.env.FORMIO_URL || config.FORMIO_URL,
};
