let config;
if (!process.env.NODE_ENV) {
  config = require('../../config.dev');
}

module.exports = {
  redis_url: process.env.REDIS_URL || config.REDIS_URL,
  datastores: {
    default: {
      adapter: 'sails-postgresql',
      port: 5432,
      host: process.env.DATABASE_HOST || config.DATABASE_HOST,
      database: process.env.DATABASE_NAME || config.DATABASE_NAME,
      user: process.env.DATABASE_USER || config.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD || config.DATABASE_PASSWORD,
    },
    mongoDB: {
      adapter: 'sails-mongo',
      url: process.env.MONGO_URL || config.MONGO_URL,
    },
  },

  models: {
    migrate: 'safe',
  },

  blueprints: {
    shortcuts: false,
  },

  security: {
    cors: {
      allRoutes: true,
      allowOrigins: '*',
      allowCredentials: false,
      allowRequestHeaders: 'content-type, authorization_token',
    },
  },

  session: {
    cookie: {
      maxAge: 24 * 60 * 60 * 1000,
    },
  },

  sockets: {
    onlyAllowOrigins: ['http://13.235.224.126:1337', 'http://13.235.224.126:9001'],
  },

  log: {
    level: 'debug',
  },

  http: {
    cache: 365.25 * 24 * 60 * 60 * 1000,
  },

  custom: {
    JWT_SECRET_KEY: process.env.JWT_SECRET_KEY || config.JWT_SECRET_KEY,
    SMS_SECRET_KEY: process.env.SMS_SECRET_KEY || config.SMS_SECRET_KEY,
    BASE_URL: process.env.BASE_URL || config.BASE_URL,
    CMS_PORT: process.env.CMS_PORT || config.CMS_PORT,
    FORMIO_PORT: process.env.FORMIO_PORT || config.FORMIO_PORT,
    FORMIO_EMAIL: process.env.FORMIO_EMAIL || config.FORMIO_EMAIL,
    FORMIO_PASSWORD: process.env.FORMIO_PASSWORD || config.FORMIO_PASSWORD,
  },
};
