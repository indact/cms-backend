let config;
if (!process.env.NODE_ENV) {
  config = require('../../config.dev');
}

module.exports = {
  redis_url: process.env.REDIS_URL || config.REDIS_URL,
};
