/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {
  /** *************************************************************************
   *                                                                          *
   * Default policy for all controllers and actions, unless overridden.       *
   * (`true` allows public access)                                            *
   *                                                                          *
   ************************************************************************** */
  // '*': true,
  AuthController: {
    '*': true,
    resetPassword: 'isLoggedIn',
    changePassword: 'isLoggedIn',
  },
  UsersController: {
    '*': 'isLoggedIn',
    create: true,
  },
  TaskTypeController: {
    '*': 'isLoggedIn',
  },
  TasksController: {
    '*': 'isLoggedIn',
    missedCall: true,
    download: true,
  },
  'formio/FormController': {
    '*': 'isLoggedIn',
    findOne: true,
  },
  'formio/SubmissionController': {
    '*': 'isLoggedIn',
    anonymous: true,
  },
  CyclesController: {
    '*': 'isLoggedIn',
  },
  BulkImportDataController: {
    '*': 'isLoggedIn',
  },
  DataController: {
    '*': 'isLoggedIn',
  },
  CovidController: {
    '*': 'covid',
    register: true,
    login: true,
    getLanguages: true,
    createLanguages: true,
    createStates: true,
    webhook: true,
    bulkRegister: true,
    getListOfStates: true,
    sendPasswordRecoveryEmail: true,
  },
  ExotelController: {
    '*': 'covid',
    callMaskingStatusCallback: true,
  },
};
