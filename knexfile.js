// Update with your config settings.
const path = require('path');

let config;
if (!process.env.NODE_ENV) {
  config = require('./config.dev');
}

const DATABASE_HOST = process.env.DATABASE_HOST || config.DATABASE_HOST;
const DATABASE_NAME = process.env.DATABASE_NAME || config.DATABASE_NAME;
const DATABASE_USER = process.env.DATABASE_USER || config.DATABASE_USER;
const DATABASE_PASSWORD = process.env.DATABASE_PASSWORD || config.DATABASE_PASSWORD;
const NODE_ENV = process.env.NODE_ENV || config.NODE_ENV;
const POOL_MAX = process.env.POOL_MAX || config.POOL_MAX;
const POOL_MIN = process.env.POOL_MIN || config.POOL_MIN;

const migrations = {
  tableName: 'knex_migrations',
  directory: path.normalize(path.join(__dirname, '/migrations')),
};

const seeds = {
  directory: path.normalize(path.join(__dirname, `/seeds/${NODE_ENV}`)),
};

module.exports[NODE_ENV] = {
  client: 'pg',
  connection: {
    host: DATABASE_HOST,
    database: DATABASE_NAME,
    user: DATABASE_USER,
    password: DATABASE_PASSWORD,
  },
  // pool: {
  //   min: POOL_MIN,
  //   max: POOL_MAX,
  // },
  seeds,
  migrations,
};
