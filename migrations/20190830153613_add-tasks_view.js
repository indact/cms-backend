const addTasksView = (knex, Promise) => knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"').then(() => Promise.all([
  knex.raw(`
  CREATE OR REPLACE VIEW tasks_view AS 
    SELECT t.id, t.deleted, t.created_at, t.updated_at,
    tt.name AS task_type_name, tt.id AS task_type_id, tt.form AS task_type_form,
    ts.id AS task_status_id, ts.name AS task_status_name, ts.category AS task_status_category,
    u.id AS assignee_id, u.mobile AS assignee_mobile, u.email AS assignee_email,
    b.id AS beneficiary_id, b.mobile AS beneficiary_mobile, b.config AS beneficiary_config,
    s.id AS state_id, s.name AS state_name, t.submission_id
    FROM tasks t
      JOIN task_type tt ON t.type = tt.id
      JOIN tasks_status ts ON t.status = ts.id
      JOIN users u ON t.assignee = u.id
      JOIN beneficiaries b ON t.beneficiary = b.id
      JOIN states s ON s.id = t.state
    WHERE t.deleted = false AND tt.deleted = false AND ts.deleted = false AND u.deleted = false AND b.deleted = false AND s.deleted = false AND tt.state = t.state AND ts.state = t.state AND b.state = t.state;
  `),
]));

const dropTasksView = (knex, Promise) => Promise.all([knex.raw(`DROP VIEW tasks_view;`)]);

exports.up = addTasksView;

exports.down = dropTasksView;
