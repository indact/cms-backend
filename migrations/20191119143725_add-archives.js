const addUserTable = (knex, Promise) => knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"').then(() => Promise.all([
  knex.schema.createTable('archive', (table) => {
    table
          .uuid('id')
          .primary()
          .defaultTo(knex.raw('uuid_generate_v4()'));
    table.json('originalRecord');
    table.json('originalRecordId');
    table.text('fromModel');
    table.bigInteger('createdat');
  }),
]));

const dropUserTable = (knex, Promise) => Promise.all([knex.schema.dropTable('archive')]);

exports.up = addUserTable;

exports.down = dropUserTable;
