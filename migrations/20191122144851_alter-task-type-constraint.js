const alterTaskTypeTable = knex => knex.raw(
    `ALTER TABLE ONLY task_type
    DROP CONSTRAINT task_type_sequence_cycle_unique;

    ALTER TABLE ONLY task_type
    DROP CONSTRAINT task_type_name_state_unique;

    CREATE UNIQUE INDEX task_type_name_cycle_state_unique ON task_type (name, cycle, state);

    CREATE UNIQUE INDEX task_type_sequence_cycle_state_unique ON task_type (sequence, cycle, state);
`,
);

const dropAlter = knex => knex.raw(
    `ALTER TABLE ONLY task_type
    DROP CONSTRAINT task_type_name_cycle_state_unique;

    ALTER TABLE ONLY task_type
    DROP CONSTRAINT task_type_sequence_cycle_state_unique;

    CREATE UNIQUE INDEX task_type_sequence_cycle_unique ON task_type (sequence, cycle);
    
    CREATE UNIQUE INDEX task_type_name_state_unique ON task_type (name, state);
`,
);

exports.up = alterTaskTypeTable;

exports.down = dropAlter;
