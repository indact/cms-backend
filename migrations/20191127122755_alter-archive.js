const alterArchiveTable = knex => knex.schema.table('archive', (table) => {
  table.dropColumn('createdat');
  table.timestamp('created_at').defaultTo(knex.fn.now());
  table.timestamp('updated_at').defaultTo(knex.fn.now());
});

const dropAlter = knex => knex.schema.table('archive', (table) => {
  table.dropColumn('created_at');
  table.dropColumn('updated_at');
  table.bigInteger('createdat');
});

exports.up = alterArchiveTable;

exports.down = dropAlter;
