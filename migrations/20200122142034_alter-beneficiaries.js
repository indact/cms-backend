const alterBeneficiaryTable = knex => knex.raw(`
    ALTER TABLE ONLY beneficiaries
    DROP CONSTRAINT beneficiaries_source_check;

    ALTER TABLE ONLY beneficiaries
    ADD CONSTRAINT beneficiaries_source_check
    CHECK ("source" = ANY (ARRAY['Exotel'::text, 'Ground Survey'::text, 'Bulk Import'::text]));
`);

const dropAlter = knex => knex.raw(`
  ALTER TABLE ONLY beneficiaries
  DROP CONSTRAINT beneficiaries_source_check;

  ALTER TABLE ONLY beneficiaries
  ADD CONSTRAINT beneficiaries_source_check
  CHECK ("source" = ANY (ARRAY['Exotel'::text, 'Ground Survey'::text]));
`);

exports.up = alterBeneficiaryTable;

exports.down = dropAlter;
