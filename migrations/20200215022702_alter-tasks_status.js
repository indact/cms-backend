const alterTaskStatusTable = knex => knex.schema.table('tasks_status', (table) => {
  table.unique(['name', 'state']);
});

const dropAlter = knex => knex.raw(`ALTER TABLE "users" DROP CONSTRAINT "tasks_status_name_state_key";`);

exports.up = alterTaskStatusTable;

exports.down = dropAlter;
