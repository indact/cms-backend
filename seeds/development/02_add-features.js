const features = [
  {
    name: 'Caller Management',
    description: 'Feature for managing the internal callers of the application',
  },
  {
    name: 'Calling Bank Management',
    description: 'Feature for managing the external calling banks of the application',
  },
  {
    name: 'Chatbot Management',
    description: 'Feature for managing the chatbot settings of the application',
  },
  {
    name: 'Feature Management',
    description: 'Feature for managing the features of the user',
  },
  {
    name: 'Form Management',
    description: 'Feature for managing the forms of the application',
  },
  {
    name: 'Missed Call Assignment',
    description: 'Feature for the Auto allocation of missed calls.',
  },
  {
    name: 'Report Generator',
    description: 'Feature to prepare the report format and generate the report.',
  },
  {
    name: 'Task Management',
    description: 'Feature for managing the tasks of the application',
  },
  {
    name: 'User Management',
    description: 'Feature for managing the users of the application',
  },
];

const seedFeatures = knex => knex('features')
    .then(() => {
      const query = knex('features').insert([...features]);
      return knex.raw(`? ON CONFLICT (name) DO NOTHING`, [query]).then(() => true);
    })
    .catch(() => '');

exports.seed = seedFeatures;
