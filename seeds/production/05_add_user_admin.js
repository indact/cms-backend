const prepareConfig = async function (knex) {
  const featureList = (await knex.from('features').select('id')).map(({ id }) => id);
  const stateList = (await knex.from('states').select('id')).map(({ id }) => id);
  const features = featureList.map(featureId => ({
    featureId,
    permissions: ['View', 'Create', 'Download', 'Delete', 'Update'],
  }));
  const featureConfig = stateList.map(state => ({
    state,
    features,
  }));
  return featureConfig;
};

const seedUser = knex => knex('users')
    .then(async () => {
      featureConfig = await prepareConfig(knex);
      return featureConfig;
    })
    .then((featureConfig) => {
      const query = knex('users').insert([
        {
          name: 'admin',
          email: 'itsupport@indusaction.org',
          mobile: '8119805204',
          password: '$2a$10$XXkscWYOcSog0KUD90VFFelKv12B9JXnE9uLgD8eJ590cZqKSG9j6',
          config: { featureConfig },
        },
      ]);
      return knex
        .raw(
          `? ON CONFLICT (email) DO UPDATE SET config = '${JSON.stringify({ featureConfig })}'`,
          [query],
        )
        .then(() => true);
    })
    .catch(() => {});
exports.seed = seedUser;
